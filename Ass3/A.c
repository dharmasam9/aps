#include <stdio.h>
#include <stdlib.h>

int DEBUG = 0;

void quick_sort(int* a, int* b, int start, int end){
	if(end-start <= 1){
		if(start < end){
			// Array of size 2
			int temp;
			if(a[start] > a[end]){
				// Size 2 not sorted
				temp = a[start];
				a[start] = a[end];
				a[end] = temp;

				temp = b[start];
				b[start] = b[end];
				b[end] = temp;
			}
		}
	}else{
		int pivot = start + rand()%(end-start+1);
		int temp;

		// Exchange pivot and start;
		temp = a[pivot];
		a[pivot] = a[start];
		a[start] = temp;

		temp = b[pivot];
		b[pivot] = b[start];
		b[start] = temp;


		pivot = start;

		int running_index = pivot;
		int last_index = pivot;
		int pivot_value = a[pivot];		
		int b_value = b[pivot];

		while(running_index <= end){
			if(a[running_index] < pivot_value){
				last_index++;

				temp = a[running_index];
				a[running_index] = a[last_index];
				a[last_index] = temp;

				temp = b[running_index];
				b[running_index] = b[last_index];
				b[last_index] = temp;
			}
			running_index++;
		}

		a[start] = a[last_index];
		a[last_index] = pivot_value;

		b[start] = b[last_index];
		b[last_index] = b_value;

		quick_sort(a, b, start, last_index-1);
		quick_sort(a, b, last_index+1, end);

	}
}

typedef struct node{
	int arrival_time;
	int cooking_time;
	struct node* next;
}node;

node* insert_to_sorted_list(node* root, int arrival_time, int cooking_time){
	if(root == NULL){
		root = (node*) calloc(1,sizeof(node));
		root->arrival_time = arrival_time;
		root->cooking_time = cooking_time;
		return root;
	}else{
		node* temp = (node*) calloc(1,sizeof(node));
		temp->arrival_time = arrival_time;
		temp->cooking_time = cooking_time;

		// Inserting in the front
		if(root->cooking_time > cooking_time){
			temp->next = root;
			root = temp;
			return root;
		}

		node* insert_pos = root;
		node* insert_pos_prev = root;
		while(insert_pos != NULL && insert_pos->cooking_time <= cooking_time){
			insert_pos_prev = insert_pos;
			insert_pos = insert_pos->next;
		}

		
		temp->next = insert_pos;
		insert_pos_prev->next = temp;
		return root;
	}
}

void print_list(node* root){
	node* temp = root;
	while(temp != NULL){
		printf("(%d %d),", temp->arrival_time, temp->cooking_time);
		temp = temp->next;
	}
	printf("\n");
}

int main(int argc, char const *argv[])
{
	int C;
	scanf("%d",&C);

	int arrivals[C];
	int cooking_times[C];

	int i;
	for (i = 0; i < C; ++i)
	{
		scanf("%d",&arrivals[i]);
		scanf("%d",&cooking_times[i]);
	}

	//quick_sort(arrivals, cooking_times, 0, C-1);
	
	node* sorted_list = NULL;
	int sorted_list_count = 0;

	int current_time = 0;
	int cust_processed = 0;
	int index = 0;
	int turn_around_time = 0;

	if(DEBUG)
		print_list(sorted_list);
	
	
	while(cust_processed != C){
		
		if(sorted_list_count == 0){
			int temp = arrivals[index];

			sorted_list = insert_to_sorted_list(sorted_list, arrivals[index], cooking_times[index]);
			sorted_list_count++;
			index++;

			while(index < C && arrivals[index] <= temp){
				// Add all element with arrival_time temp to sorted_list
				sorted_list = insert_to_sorted_list(sorted_list, arrivals[index], cooking_times[index]);
				sorted_list_count++;
				index++;
			}
			current_time = temp;
		}

		// Getting the minimum
		int at = sorted_list->arrival_time;
		int ct = sorted_list->cooking_time;

		sorted_list = sorted_list->next;
		sorted_list_count--;

		

		// Catering the customer
		//printf("%d\n", ct);
		current_time += ct;
		cust_processed++;
		turn_around_time += current_time-at;


		// Inserting more customers till current_time
		while(index < C && arrivals[index] <= current_time){
			sorted_list = insert_to_sorted_list(sorted_list, arrivals[index], cooking_times[index]);
			sorted_list_count++;
			index++;
		}
		
		
	}

	printf("%d\n", turn_around_time/C);


	return 0;
}
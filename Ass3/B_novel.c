#include <stdio.h>
#include <stdlib.h>

void print_array(int* a, int n){
	int i;
	for (i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);

	while(tc > 0){
		int n;
		scanf("%d",&n);

		int values[n];
		int* cups = (int*)calloc(n,sizeof(int));

		int i;
		for (i = 0; i < n; ++i){
			scanf("%d",&values[i]);
			cups[i] = 1;
		}

		int cur_index;
		int cup_value;

		if(values[0] > values[1]){
			cur_index = 0;
			cup_value = 1;

			while(values[cur_index] > values[cur_index+1] && cur_index < n-1){
				cur_index++;
				cup_value++;
			}
			cups[0] = cup_value;
		}


		int index;
		for ( index = 1; index < n-1; ++index)
		{
			if(values[index] > values[index+1] && values[index] > values[index-1]){
				// Both are greater than current index
				cur_index = index;
				cup_value = 1;

				while(values[cur_index] > values[cur_index+1] && cur_index < n-1){
					cur_index++;
					cup_value++;
				}

				if(cups[index-1] >= cup_value){
					cups[index] = cups[index-1]+1;
				}else{
					cups[index] = cup_value;
				}

			}else if(values[index] > values[index-1]){
				cups[index] = cups[index-1] + 1;
			}
			else if(values[index] > values[index+1]){
				cur_index = index;
				cup_value = 1;

				while(values[cur_index] > values[cur_index+1] && cur_index < n-1){
					cur_index++;
					cup_value++;
				}

				cups[index] = cup_value;
			}

//			print_array(cups,n);

		}


		if(values[n-1] > values[n-2]){
			cups[n-1] = cups[n-2] + 1;
		}



		long long int sum = 0;
		for (i = 0; i < n; ++i)
		{
			sum += cups[i];
		}

		printf("%lld\n", sum);

		

		tc--;
	}

	return 0;
}
#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int value;
	struct node* left;
	struct node* right;
}node;


node* convert_to_tree(int* values,int start, int end){
	if(start <= end){
		node* temp = (node*)calloc(1,sizeof(node));
		temp->value = values[start];
		int right_start = start+1;
		int temp_value = values[start];

		while(right_start <= end && values[right_start] < temp_value){
			right_start++;
		}

		temp->left = convert_to_tree(values,start+1,right_start-1);
		temp->right = convert_to_tree(values,right_start,end);

		return temp;

	}else{
		return NULL;
	}

}

void print_post_order(node* root){
	if(root != NULL){
		print_post_order(root->left);
		print_post_order(root->right);
		printf("%d\n", root->value);
	}
}

int get_minumum_and_check(node* root){
	if(root->left != NULL){
		if(root->right != NULL){
			int left_min = get_minumum_and_check(root->left);
			int right_min = get_minumum_and_check(root->right);

			if(root->value >= left_min && root->value <= right_min){
				return left_min;
			}else{
				return -1;
			}

		}else{
			int left_min = get_minumum_and_check(root->left);
			if(root->value >= left_min){
				return left_min;
			}else{
				return -1;
			}
		}
	}else{
		if(root->right != NULL){
			int right_min = get_minumum_and_check(root->right);

			if(root->value <= right_min){
				return root->value;
			}else{
				return -1;
			}
		}else{
			return root->value;
		}
	}
	

}


int main(){
	int tc;
	int tc_copy;
	scanf("%d",&tc);
	tc_copy = tc;

	while(tc > 0){
		int n;
		scanf("%d",&n);

		int values[n];
		int i;
		for (i = 0; i < n; ++i)
			scanf("%d",&values[i]);

		node* root = convert_to_tree(values, 0, n-1);
		printf("Case %d:\n", tc_copy-tc+1);

//		position = 0;
//		int sort_array[n];
//		get_in_order_array(root,sort_array);

		int ret = get_minumum_and_check(root);

		if(ret != -1)
			print_post_order(root);
		else
			printf("-1\n");

		tc--;
	}
	return 0;
}
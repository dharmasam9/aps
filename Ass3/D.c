#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);

	while(tc > 0){
		int n;
		scanf("%d",&n);

		int evid[n];
		int i;

		int stack[n];
		int stackIndex = -1;
		int next_val = 1;

		for (i = 0; i < n; ++i)
			scanf("%d",&evid[i]);

		int possible = 1;
		for (i = 0; i < n && possible; ++i){
			if(evid[i] >= next_val){
				int j;
				for (j = next_val; j <= evid[i]; ++j)
				{
					stackIndex++;
					stack[stackIndex] = j;
				}
				next_val = evid[i]+1;
			}

			if(!(stackIndex != -1 && stack[stackIndex] == evid[i])){
				possible = 0;
			}else{
				stackIndex--;
			}
		}

		if(possible)
			printf("Yes\n");
		else
			printf("No\n");

		tc--;
	}
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int max_int(int a,int b){
	if(a > b)
		return a;
	else
		return b;
}

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);

	while(tc > 0){
		int m,n;
		scanf("%d",&m);
		scanf("%d",&n);

		int i,j,k;
		int values[m][n];
		int count = 0;
		int max = INT_MIN;
		for(i = 0; i < m; ++i)
		{
			for(j = 0; j < n; ++j)
			{
				scanf("%d",&values[i][j]);
				if(values[i][j] < 0){
					count++;
					if(values[i][j] > max)
						max = values[i][j];
				}
			}
		}

		if(count == m*n){
			printf("0\n");
			continue;
		}

		int temp[m];

		int cur_sum, max_sum;
		cur_sum = 0;
		max_sum = 0;
		for (i = 0; i < n; ++i)
		{	
			for (j = 0; j < m; ++j)
				temp[j] = 0;

			for (k = i; k < n; ++k)
			{
				for (j = 0; j < m; ++j)
					temp[j] += values[j][k];

				int sum = 0;
				cur_sum = 0;

				// Find max sum in temp array
				for (j = 0; j < m; ++j)
				{
					if(sum + temp[j] < 0){
						sum = 0;
					}else{
						sum += temp[j];
					}

					cur_sum = max_int(cur_sum,sum);
					//printf("here %d\n",cur_sum );
				}

				/*
				printf("%d %d %d\n", i,k,cur_sum);
				for (j = 0; j < m; ++j)
					printf("%d\n", temp[j]);
				printf("\n");
				*/

				max_sum = max_int(max_sum,cur_sum);
			}
		}

		printf("%d\n", max_sum);




		tc--;
	}

	return 0;
}
// dMP7Ah
#include <iostream>
#include <vector>
#include <queue>

#define WHITE 0
#define GREY 1
#define BLACK 2

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	int c;
	int s1,s2;

	cin >> n >> c;

	vector<vector<int> > adjacencyList(n);

	for (int i = 0; i < c; ++i)
	{
		cin >> s1 >> s2;
		adjacencyList[s1].push_back(s2);
		adjacencyList[s2].push_back(s1);
	}

	// queue for bfs
	queue<int> vertex_queue;
	vector<int> vertex_colors(n,WHITE);

	long int exchanges = 1;

	// Loop through all vertices
	for (int i = 0; i < n; ++i)
	{
		int group_count;
		if(vertex_colors[i] == 0){
			// New vertex found

			group_count = 1;
			vertex_queue.push(i);
			vertex_colors[i] = GREY;

			int neighbour;
			int selected_vertex;
			while(!vertex_queue.empty()){
				selected_vertex = vertex_queue.front();
				for (unsigned int j = 0; j < adjacencyList[selected_vertex].size(); ++j)
				{
					neighbour = adjacencyList[selected_vertex][j];
					if(vertex_colors[neighbour] == WHITE){
						vertex_queue.push(neighbour);
						vertex_colors[neighbour] = GREY;

						group_count++;
						exchanges = (exchanges*group_count)%(1000000007);

					}
				}

				vertex_colors[selected_vertex] = BLACK;
				vertex_queue.pop();
			}			
			
		}
	}

	cout << exchanges;






	return 0;
}
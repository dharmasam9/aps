#include <iostream>
#include <queue>
#include <string>
#include <utility>

using namespace std;

int matrix[1000][1000];
int max_paths[1000][1000];

int find_maximum_path(int start_row, int start_col,int rows, int cols){
	int next_row;
	int next_col;

	int max = 1;
	int current_value = matrix[start_row][start_col];
	int ret_val;
	
	for (int i = -1; i <=1; ++i)
	{
		next_row = start_row + i;
		if(next_row >= 0 && next_row < rows){
			// Possible to go to next row
			for (int j = -1; j <=1; ++j)
			{
				next_col = start_col + j;
				if(next_col >= 0 && next_col < cols){
					// That neighbour is accessible
					if(i !=0 || j != 0){
						// Not itself
						if(matrix[next_row][next_col]- current_value == 1){
							//cout << next_row << " " << next_col << endl;
							if(max_paths[next_row][next_col] != -1){
								ret_val = max_paths[next_row][next_col];
							}else{
								ret_val = find_maximum_path(next_row, next_col, rows, cols);
							}

							if(ret_val+1 > max){
								max = ret_val+1;
							}
						}
					}
				}
			}	
		}
	}

	max_paths[start_row][start_col] = max;
	return max;

}

int main(int argc, char const *argv[])
{

	int m,n;
	while(1){
		cin >> m >> n;
		if(m == 0 && n == 0)
			break;

		queue<pair<int,int> > start_positions;

		string s;
		for (int i = 0; i < m; ++i)
		{
			cin >> s;
			for (unsigned int j = 0; j < s.length(); ++j)
			{
					matrix[i][j] = (int)s[j];
					if(matrix[i][j] == 65){
						pair<int,int> temp(i,j);
						start_positions.push(temp);
					}
					max_paths[i][j] = -1;
			}
		}

		int max_path_length = 0;
		while(!start_positions.empty()){
			pair<int,int> start = start_positions.front();
			start_positions.pop();
			
			int row = start.first;
			int col = start.second;

			int path_length = find_maximum_path(row, col, m, n);
			max_path_length =  (path_length > max_path_length)?path_length:max_path_length;

		}

		cout << max_path_length << endl;




	}
	

	return 0;
}
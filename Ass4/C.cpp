#include <cstdio>
#include <iostream>
#include <vector>
#include <cstdlib>

#define ALPHA_SIZE 10

using namespace std;


typedef struct node{
	int is_leaf;
	int children_count;
	struct node* children[ALPHA_SIZE];
}node;


node* get_new_node(){
	node* temp = (node*) calloc(1, sizeof(node));

	temp->is_leaf = 0;
	temp->children_count = 0;

	for (int i = 0; i < ALPHA_SIZE; ++i)
	{
		temp->children[i] = NULL;
	}

	return temp;

}

void free_trie(node* root){
	if(root == NULL)
		return;
	else{
		for (int i = 0; i < ALPHA_SIZE; ++i)
		{
			free_trie(root->children[i]);
		}
		free(root);
	}
}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int n;
	string s;

	while(tc > 0){
		cin >> n;

		node* root = get_new_node();
		bool duplicate = false;

		int i;
		for (i = 0; i < n && !duplicate; ++i)
		{
			cin >> s;
			node* current_node = root;
			for (unsigned int j = 0; j < s.size() && !duplicate; ++j)
			{
				int bucket_num = (int)s[j] - (int)'0';

				if(current_node->is_leaf == 1)
					duplicate = true;

				if(current_node->children[bucket_num] == NULL){
					current_node->children[bucket_num] = get_new_node();
					current_node->children_count = current_node->children_count + 1;
				}

				current_node = current_node->children[bucket_num];
			}

			if(current_node->children_count > 0 || current_node->is_leaf == 1)
				duplicate = true;

			current_node->is_leaf = 1;			
		}

		for (int j = i; j < n; ++j)
		{
			cin >> s;
		}

		if(duplicate){
			cout << "NO" << endl;
		}else{
			cout << "YES" << endl;
		}

		free_trie(root);

		tc--;
	}


	return 0;
}
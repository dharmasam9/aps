#include <iostream>
#include <vector>
#include <stack>

#define WHITE 0
#define GREY 1
#define BLACK 2
#define EXTREME_BLACK 3

using namespace std;


void dfs_visit(int start_vertex,
				vector<vector<int> > &graph,
				vector<int> &vertex_colors, 
				stack<int> &dfs_closing_stack){

	// Making node visited
	vertex_colors[start_vertex] = GREY;

	for (unsigned int i = 0; i < graph[start_vertex].size(); ++i)
	{
		int neighbour = graph[start_vertex][i];
		if(vertex_colors[neighbour] == WHITE){
			vertex_colors[neighbour] = GREY;
			dfs_visit(neighbour, graph, vertex_colors, dfs_closing_stack);
		}
	}

	vertex_colors[start_vertex] = BLACK;
	dfs_closing_stack.push(start_vertex);
}

void reverse_dfs_visit(int start_vertex,
						vector<vector<int> > &graph, 
						vector<int> &vertex_colors, 
						vector<int> &node_comp_numbers, int scc_id){

	vertex_colors[start_vertex] = GREY;
	node_comp_numbers[start_vertex] = scc_id;

	for (unsigned int i = 0; i < graph[start_vertex].size(); ++i)
	{
		int neighbour = graph[start_vertex][i];
		if(vertex_colors[neighbour] == WHITE){
			vertex_colors[neighbour] = GREY;
			reverse_dfs_visit(neighbour, graph, vertex_colors, node_comp_numbers, scc_id);
		}
	}

	vertex_colors[start_vertex] = BLACK;
}



int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int n;
	while(tc > 0){
		cin >> n;

		vector<vector<int> > graph(n);
		vector<vector<int> > reverse_graph(n);


		int out_degree_zero_count = 0;

		int neighbour_count;
		int neighbour;
		for (int i = 0; i < n; ++i)
		{
			cin >> neighbour_count;

			if(neighbour_count == 0) out_degree_zero_count += 1;

			for (int j = 0; j < neighbour_count; ++j)
			{
				cin >> neighbour;
				neighbour--;
				graph[i].push_back(neighbour);
				reverse_graph[neighbour].push_back(i);

				// graph[neighbour].push_back(i);
				// reverse_graph[i].push_back(neighbour);
			}
		}

		// Basic check
		if(out_degree_zero_count > 1){
			cout << "0" << endl;
			tc--;
			continue;
		}

		

		// Perform DFS
		vector<int> vertex_colors(n, WHITE);
		stack<int> dfs_closing_stack;

		for (int i = 0; i < n; ++i)
		{
			if(vertex_colors[i] == WHITE){
				dfs_visit(i, graph, vertex_colors, dfs_closing_stack);				
			}
		}
		
		// DFS on reverse	
		vector<int> rev_vertex_colors(n, WHITE);
		vector<int> node_comp_numbers(n,-1);

		int scc_id = 0;
		
		while(!dfs_closing_stack.empty()){

			int start_vertex = dfs_closing_stack.top();
			dfs_closing_stack.pop();

			if(rev_vertex_colors[start_vertex] == WHITE){
				reverse_dfs_visit(start_vertex, 
					reverse_graph, 
					rev_vertex_colors, 
					node_comp_numbers, scc_id);				
				scc_id++;
			}
			
		}

		int num_comp = scc_id;

		vector<int> in_degrees(num_comp,0);

		for (int i = 0; i < n; ++i)
		{
			for (unsigned int j = 0; j < reverse_graph[i].size(); ++j)
			{
				if(node_comp_numbers[i] != node_comp_numbers[reverse_graph[i][j]]){
					in_degrees[node_comp_numbers[reverse_graph[i][j]]]++;
				}
			}
		}


		int count = 0;
		for (int i = 0; i < num_comp; ++i)
		{
			if(in_degrees[i] == 0){
				count++;
			}
		}

		if(count > 1){
			cout << "0" << endl;
		}else{
			if(count == 0){
				cout << n << endl;
			}else{
				for (int i = 0; i < num_comp; ++i)
				{
					if(in_degrees[i] == 0){
						int nodes_in_comp = 0;
						for (int j = 0; j < n; ++j)
						{
							if(node_comp_numbers[j] == i)
								nodes_in_comp++;
						}
						cout << nodes_in_comp << endl;
					}
				}
			}
		}
		

		tc--;
	}

	return 0;
}
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	char s[1001];

	while(tc > 0){
		int n;
		cin >> n;

		vector<int> sequence(n);
		int temp;
		for (int i = 0; i < n; ++i)
		{
			cin >> temp;
			temp--;
			sequence[i] = temp;
		}

		int matrix[n][n];
		int edge_count;
		for (int i = 0; i < n; ++i)
		{
			cin >> s;
			for (int j = 0; j < n; ++j)
			{
				matrix[i][j] = (int)s[j] - 48;
				edge_count += matrix[i][j];
			}
		}

		/*
		for (int i = 0; i < n; ++i)
			cout << endl;

		for (int i = 0; i < n; ++i)
			cout << sequence[i] << " ";
			cout << endl;
		*/

		// Do bfs and sort in that set.

		vector<int> colors(n, 0);
		queue<int> node_queue;

		for (int i = 0; i < n; ++i)
		{
			if(colors[i] == 0){
				vector<int> indices;
				vector<int> nodes;

				colors[i] = 1;
				node_queue.push(i);

				while(!node_queue.empty()){
					int current_node = node_queue.front();
					node_queue.pop();
					
					indices.push_back(current_node);
					nodes.push_back(sequence[current_node]);

					for (int j = 0; j < n; ++j)
					{
						if(matrix[current_node][j] == 1 && colors[j] == 0){
							colors[j] = 1;
							node_queue.push(j);
						}
					}
				}

				sort(indices.begin(), indices.end());
				sort(nodes.begin(), nodes.end());

				for (unsigned int i = 0; i < nodes.size(); ++i)
				{
					sequence[indices[i]] = nodes[i];
				}

			}
		}

		for (unsigned int i = 0; i < sequence.size(); ++i)
			cout << sequence[i]+1 << " ";
			cout << endl;


		
		tc--;
	}
	return 0;
}
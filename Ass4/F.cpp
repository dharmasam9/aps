#include <iostream>
#include <vector>
#include <stack>


#define WHITE 0
#define GREY 1
#define BLACK 2
#define EXTREME_BLACK 3


using namespace std;

void dfs_visit(int start_vertex,
				vector<vector<int> > &graph,
				vector<int> &vertex_colors,
				bool &has_cycle,
				stack<int> &top_sort){

	// Making node visited
	vertex_colors[start_vertex] = GREY;

	for (unsigned int i = 0; i < graph[start_vertex].size(); ++i)
	{
		int neighbour = graph[start_vertex][i];
		if(vertex_colors[neighbour] == WHITE){
			vertex_colors[neighbour] = GREY;
			dfs_visit(neighbour, graph, vertex_colors, has_cycle, top_sort);
		}else if(vertex_colors[neighbour] == GREY){
			has_cycle = true;
			return;
		}
	}

	vertex_colors[start_vertex] = BLACK;
	top_sort.push(start_vertex);
}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int c,p;
		cin >> c >> p;

		vector<vector<int> > graph(c);
		vector<int> indegree_count(c, 0);

		int n1,n2;
		for (int i = 0; i < p; ++i)
		{
			cin >> n1 >> n2;
			n1--;
			n2--;
			indegree_count[n2] += 1;
			graph[n1].push_back(n2);
		}

		vector<int> vertex_colors(c, WHITE);
		bool has_cycle = false;
		stack<int> top_sort;

		int num_start_nodes = 0;

		for (int i = 0; i < c; ++i)
		{
			if(indegree_count[i] == 0){
				num_start_nodes++;
				//A start node
				dfs_visit(i, graph, vertex_colors, has_cycle, top_sort);
			}
		}

		// Check if any color is white, incase a subgraph is a cycle
		for (int i = 0; i < c && !has_cycle; ++i)
		{
			if(vertex_colors[i] == WHITE)
				has_cycle = true;
		}


		if(has_cycle){
			cout << "valar morghulis" << endl;
		}else{
			if(num_start_nodes > 1){
				cout << "valar dohaeris" << endl;
			}else{
				// Finding multiple ways with in single graph
				// Start from top of the stack and check whether next one in the 
				// list is in the neighbour or not
				vector<int> copy_stack;

				// Copying stack to vector
				while(!top_sort.empty()){
					copy_stack.push_back(top_sort.top());
					top_sort.pop();
				}

				int current_vertex;
				int next_vertex;

				bool next_is_neigh = true;

				for (unsigned int i = 0; i < copy_stack.size()-1 && next_is_neigh; ++i)
				{
					current_vertex = copy_stack[i];
					next_vertex = copy_stack[i+1];

					next_is_neigh = false;

					for (unsigned int j = 0; j < graph[current_vertex].size() && !next_is_neigh; ++j)
					{
						if(graph[current_vertex][j] == next_vertex){
							next_is_neigh = true;
						}
					}
				}

				if(next_is_neigh){
					int temp_value;
					for (unsigned int i = 0; i < copy_stack.size(); ++i){
						temp_value = copy_stack[i]+1;
						cout << temp_value;
						if(i != copy_stack.size()-1)
							cout << " ";
					}
					cout << endl;
				}else{
					cout << "valar dohaeris" << endl;
				}


			}
		}


		tc--;
	}

	return 0;
}
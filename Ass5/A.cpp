//FLoE3R
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int counts[1001];

	while(tc > 0){
		int n,m;
		cin >> n >> m;

		int temp;

		// Making them zero
		for (int i = 0; i < m; ++i)
			counts[i] = 0;

		// Reading input.
		for (int i = 0; i < n; ++i)
		{
			cin >> temp;	
			counts[temp%m]++;
		}

		// Two boolean arrays
		bool* b_array1 = new bool[m];
		bool* b_array2 = new bool[m];
		bool* b_temp;

		int count, segment;
		int weight, determinant;
		for (int i = 0; i < m; ++i)
		{
			count = counts[i];
			segment = 1;

			while(count > 0){
				// Swapping two boolean arrays.
				b_temp = b_array1;
				b_array1 = b_array2;
				b_array2 = b_temp;

				// Falsing b_array2
				for (int j = 0; j < m; ++j)
					b_array2[j] = false;

				determinant = min(count, segment);
				weight = (determinant*i) - ((determinant*i)/m)*m;

				for (int k = 0; k < m; ++k)
				{
					temp = (k+weight) - ((k+weight)/m)*m;
					b_array2[temp] = b_array2[temp] | b_array1[k];
				}

				for (int l = 0; l < m; ++l)
				{
					b_array2[l] = b_array2[l] | b_array1[l];
				}

				b_array2[weight] = true;
				count = count - determinant;

				segment *= 2;

			}
		}


		if(b_array2[0])
			cout << "YES" << endl;
		else
			cout << "NO" << endl;



		tc--;
	}

	return 0;
}
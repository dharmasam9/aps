#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;

		int* values = new int[n+2];
		for (int i = 0; i < n; ++i)
		{
			cin >> values[i];
		}
		

		long int* max_saving = new long int[n+6];
		long int value1,value2,value3;

		for (int i = n-1; i >=0; i--)
		{
			
			value1 = values[i] + max_saving[i+2];
			value2 = values[i] + values[i+1] + max_saving[i+4];
			value3 = values[i] + values[i+1] + values[i+2] + max_saving[i+6];

			if(value1 < value2)
				value1 = value2;

			if(value1 < value3)
				value1 = value3;

			max_saving[i] = value1;	
		}

		cout << max_saving[0] << endl;
		


		


		tc--;
	}
	return 0;
}
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	
	int tc;
	cin >> tc;

	long int* values = new long int[1000001];
	values[0] = 1;
	values[1] = 1;
	values[2] = 2;
	values[3] = 4;

	int value_found = 3;

	while(tc > 0){
		int n;
		cin >> n;

		if(n > value_found){
			long int temp;
			for (int i = value_found+1; i <= n; ++i)
			{
				temp = values[i-1] + values[i-2] + values[i-3];
				values[i] = temp%1000000007;
			}
		}

		cout << values[n] << endl;

		tc--;
	}

	return 0;
}
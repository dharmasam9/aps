#include <iostream>

using namespace std;


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;

		// Allocating memory
		int** values = new int*[n];
		for (int i = 0; i < n; ++i)
			values[i] = new int[n];

		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j <= i; ++j)
			{
				cin >> values[i][j];
			}
		}

		if(n <= 2){
			if(n==1)
				cout << values[0][0] << endl;
			else
				if(values[1][0] < values[1][1])
					cout << values[0][0] + values[1][0] << endl;
				else
					cout << values[0][0] + values[1][1] << endl;
		}else{

			values[1][0] += values[0][0];
			values[1][1] += values[0][0];

			int min_value;

			for (int i = 2; i < n; ++i)
			{

				// First element
				if(values[i-1][0] < values[i-1][1]){
					values[i][0] += values[i-1][0];
				}else{
					values[i][0] += values[i-1][1];
				}

				// From 1:i-2
				for (int j = 1; j <= i-2; ++j)
				{
					min_value = values[i-1][j-1];

					if(values[i-1][j] < min_value)	
						min_value = values[i-1][j];

					if(values[i-1][j+1] < min_value)
						min_value = values[i-1][j+1];
					
					values[i][j] += min_value;

				}

				// Last before element.

				if(values[i-1][i-2] < values[i-1][i-1])
					values[i][i-1] += values[i-1][i-2];
				else
					values[i][i-1] += values[i-1][i-1];

				// Last element
				values[i][i] += values[i-1][i-1];

			}

			min_value = values[n-1][0];
			for (int i = 1; i < n; ++i)
			{
				if(values[n-1][i] < min_value)
					min_value = values[n-1][i];
			}
			cout << min_value << endl;
		}

		




		tc--;
	}
	return 0;
}
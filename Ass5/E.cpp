
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int L,K,N;
	int tc;
	int temp;

	cin >> L >> K >> tc;

	if(L > K){
		temp = L;
		L = K;
		K = temp;
	}

	int winners[1000001];

	// Setting up base cases.
	winners[0] = 0;
	winners[1] = 0;
	
	for (int i = 2; i < L; ++i)
	{
		if(i%2 == 0)
			winners[i] = 1;
		else
			winners[i] = 0;
	}
	winners[L] = 0;

	// For L+1 to K-1
	for (int i = L+1; i < K; ++i)
	{
		if(winners[i-1] == winners[i-L]){
			winners[i] = (winners[i-1]+1)%2;
		}else{
			winners[i] = winners[i-1];
		}
	}
	winners[K] = 0;

	int present_n = K;


	while(tc > 0)
	{
		cin >> N;

		if(N <= present_n){
			cout << (char)(winners[N]+65);
		}else{
			for (int i = present_n+1; i <= N; ++i)
			{
				if((winners[i-1] == winners[i-L]) && (winners[i-1] == winners[i-K])){
					winners[i] = (winners[i-1]+1)%2;
				}else{
					winners[i] = 0;
				}
			}

			present_n = N;
			cout << (char)(winners[N]+65);
		}
		tc--;
	}

	return 0;
}
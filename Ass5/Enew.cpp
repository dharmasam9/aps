#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int L,K,N;
	int tc;
	int rem;

	cin >> L >> K >> tc;

	while(tc > 0)
	{
		cin >> N;

		rem = N%(L+K);

		if(rem%2 == 0){
			cout << "B";
		}else{
			cout << "A";	
		}
		tc--;
	}
	cout << endl;
}
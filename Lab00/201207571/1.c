#include "stdio.h"

int main(int argc, char* argv[]){
	int x,y;
	scanf("%d %d",&x,&y);

	if(x ==0 || y==0){
		if(x != 0)
			printf("%d\n",x);
		else
			printf("%d\n",y);
		return 0;
	}

	// x should be greater than y
	int temp;
	if(y > x){
		temp = x;
		x = y;
		y = temp;
	}

	while(y != 0){
		temp = y;
		y = x%y;
		x = temp;

	}

	printf("%d\n", x);

	return 0;
}
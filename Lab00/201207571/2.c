#include "stdio.h"

int main(int argc, char* argv[]){
	int x,y;
	scanf("%d %d",&x,&y);

	if(x ==0 || y==0){
		if(x != 0)
			printf("%d\n",x);
		else
			printf("%d\n",y);
		return 0;
	}

	if(x < y){
		printf("%d\n",x);
		return 0;
	}

	while(x >= y){
		x -= y;
	}

	printf("%d %d\n", x, x%y);
	
	return 0;
}
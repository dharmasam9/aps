#include "stdio.h"

int main(int argc, char* argv[]){
	int x,y;
	scanf("%d %d",&x,&y);

	int sign = x*y;

	if(sign > 0){
		sign = 1;
	}else{
		sign = -1;
	}

	// Making x and y positive
	if(x < 0){
		x = -1*x;
	}

	if(y < 0){
		y = -1*y;
	}

	// Returning zero if x is less than y
	if(x < y){
		printf("0\n");
		return 0;
	}

	// Divide by zero error
	if(y == 0){
		printf("y cannot be zero\n");
		return 0;
	}

	int parts = 0;
	while(x >= y){
		x -= y;
		parts++;
	}

	printf("%d\n", sign*parts);

	return 0;
}
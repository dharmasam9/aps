#include "stdio.h"

int main(int argc, char* argv[]){
	int x,y;
	scanf("%d %d",&x,&y);

	int orig_x = x;
	int orig_y = y;

	if(x == 0 || y == 0){
		printf("0\n");
		return 0;
	}

	int answer = 0;
	while(y != 1){
		if(y%2 != 0){
			answer += x;
		}
		y = y>>1;
		x = x<<1;

		//printf("%d %d\n",x,y);
	}
	answer += x;


	printf("%d \n", answer);

	
	
	return 0;
}
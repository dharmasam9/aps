// 1/1-x
#include "stdio.h"

int main(int argc, char* argv[]){
	float x;
	int d;
	scanf("%f %d",&x,&d);

	float cur_term = 1.0;
	float answer = cur_term;
	
	// calculating epsilon value
	float epsilon = 1.0;
	int i;
	for (i = 0; i < d; ++i){
		epsilon *= 0.1;
	}

	float comp;
	//printf("%f\n", epsilon);
	do{
		cur_term *= x;
		answer += cur_term;
		//printf("%f %f \n", answer, cur_term);
		if(cur_term < 0){
			comp = -1*cur_term;
		}else{
			comp = cur_term;
		}

	}while(comp > epsilon);

	printf("%lf\n", answer);

	return 0;
}
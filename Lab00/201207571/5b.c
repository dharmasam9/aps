// ln (1+x)
#include "stdio.h"

int main(int argc, char* argv[]){
	float x;
	int d;
	scanf("%f %d",&x,&d);

	float cur_term = x;
	int count = 1;
	float answer = cur_term;
	
	// calculating epsilon value
	float epsilon = 1.0;
	int i;
	for (i = 0; i < d; ++i){
		epsilon *= 0.1;
	}

	float comp;
	//printf("%f\n", epsilon);
	do{
		count++;
		cur_term *= (-1*x);
		answer += (cur_term/(float)count);
		
		comp = (cur_term/(float)count);
		if(cur_term < 0){
			comp = -1*comp;
		}
		//printf("%f %f \n", answer, comp);

	}while(comp > epsilon);

	printf("%f\n", answer);

	return 0;
}
// 1/1-x
#include "stdio.h"

int main(int argc, char* argv[]){
	float x;
	int d;
	scanf("%f %d",&x,&d);

	float cur_term = 1.0;
	int count = 0;
	float answer = cur_term;
	float prev_answer = answer;
	
	// calculating epsilon value
	float epsilon = 1.0;
	int i;
	for (i = 0; i < d; ++i){
		epsilon *= 0.1;
	}

	float comp, comp1,comp2;
	//printf("%f\n", epsilon);
	do{
		count++;
		cur_term *= (x/count);
		prev_answer = answer;
		answer += cur_term;
		
		comp = answer-prev_answer;

		if(comp < 0)
			comp *= -1;

	}while(comp > epsilon);

	printf("%f\n", answer);

	return 0;
}
#include "stdio.h"

int main(int argc, char* argv[]){
	float x;
	int d;
	scanf("%f %d",&x,&d);

	float cur_term = 1;
	int count = 0;
	float answer = cur_term;
	float prev_answer = answer;
	
	// calculating epsilon value
	float epsilon = 1.0;
	int i;
	for (i = 0; i < d; ++i){
		epsilon *= 0.1;
	}

	float comp,comp1,comp2;
	//printf("%f\n", epsilon);
	do{
		count += 2;
		cur_term *= (-1*x*x)/(count*(count-1));
		
		prev_answer = answer;
		answer += cur_term;

		comp1 = answer;
		if(answer < 0) comp1 = -1*comp1;

		comp2 = prev_answer;
		if(prev_answer < 0) comp2 = -1*comp2;

		comp = comp1-comp2;
		if(comp < 0)
			comp *= -1;
		
	}while(comp > epsilon);

	printf("%f\n", answer);

	return 0;
}
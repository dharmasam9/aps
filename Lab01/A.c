#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void print_array(int* arr,int len){
	int i;
	for(i=0;i<len;i++){
		printf("%3d", arr[i]);
	}
	printf("\n");
}

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);


	char* mini = (char*) calloc(1000001, sizeof(char));
	char* madhur = (char*) calloc(1000001, sizeof(char));
	
	int* mini_count = (int*) calloc(26, sizeof(int));
	int* madhur_count = (int*) calloc(26, sizeof(int));

	int i;
	int equal;
	while(tc > 0){
		scanf("%s",mini);
		scanf("%s",madhur);

		equal = 1;
		
		// taking mini alphabet
		for (i = 0; i < strlen(mini); ++i)
			mini_count[(int)mini[i]-97]++;
		
		// taking madhur alphabet
		for (i = 0; i < strlen(madhur); ++i)
			madhur_count[(int)madhur[i]-97]++;
	
		// break when it is not equal		
		for (i = 0; i < 26; ++i){
			if(mini_count[i] != madhur_count[i])
				equal = 0;

			// resetting
			mini_count[i] = 0;
			madhur_count[i] = 0;
		}
			

		if(equal)
			printf("MINI\n");
		else
			printf("MADHUR\n");
	
		

		tc--;		
	}
	return 0;
}
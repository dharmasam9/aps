#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);

	char str1[1000001];
	char str2[1000001];

	int mini_count[26];
	int madhur_count[26];

	int i;
	int same;

	while(tc > 0){
		scanf("%s",str1);
		scanf("%s",str2);
		int len1 = strlen(str1);
		int len2 = strlen(str2);

		same = 1;

		// setting to zero
		for (i = 0; i < 26; ++i)
		{
			mini_count[i] = 0; madhur_count[i] = 0;
		}

		
		int ascii;

		for (i = 0; i < len1; ++i)
		{
			ascii = (int)str1[i] - 97;
			mini_count[ascii]++;
		}

		for (i = 0; i < len2; ++i)
		{
			ascii = (int)str2[i] - 97;
			madhur_count[ascii]++;
		}

		// checking for result
		for (i = 0; i < 26; ++i)
		{
			if(mini_count[i] != madhur_count[i]){
				same = 0;
			}
		}

		if(same)
			printf("MINI\n");
		else
			printf("MADHUR\n");



		tc--;
	}
	return 0;
}
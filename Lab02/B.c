#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void print_array(int* a,int n){
	int i;
	for (i = 0; i < n; ++i)
	{
		printf("%3d", a[i]);
	}
	printf("\n");
}


int main(int argc, char const *argv[])
{	
	//time_t t;

	int tc;
	scanf("%d",&tc);

	int* numbers;
	int n,k;

	int DEBUG = 0;

	// initializing random function
	//srand((unsigned) time(&t));
	//srand(time(NULL));

	while(tc > 0){
		// Take n and k
		scanf("%d",&n);
		numbers = (int*) calloc(n,sizeof(int));
		scanf("%d",&k);
		k = n-k; // To make it zero based index

		int i;
		for (i = 0; i < n; ++i)
			scanf("%d",&numbers[i]);

		// Find the kth largest number		
		int start = 0;
		int end = n-1;
		int rand_index;
		int temp,pivot,runner;


		while(start <= end){

			rand_index = (start + end)/2;

			//rand_index = rand()%(end-start+1) + start;

			if(DEBUG){
				printf("%d %d %d\n ", rand_index,start,end);
				print_array(numbers,n);
			}

			// swapping pivot with start
			pivot = numbers[rand_index];
			numbers[rand_index] = numbers[start];
			numbers[start] = pivot;

			runner = start;

			for (i = start+1; i <= end; ++i)
			{
				if(numbers[i] < pivot){
					// increment runner
					runner++;
					// swap
					temp = numbers[i];
					numbers[i] = numbers[runner];
					numbers[runner] = temp;
				}
			}

			// swap runner
			temp = numbers[runner];
			numbers[runner] = numbers[start];
			numbers[start] = temp;

			if(DEBUG)
				print_array(numbers,n);

			if(k == runner){
				printf("%d\n", numbers[runner]);
				break;
			}else if(k > runner){
				start = runner+1;
			}else{
				end = runner-1;
			}

		}

		if(DEBUG) printf("\n");

	
		tc--;
	}
	return 0;
}
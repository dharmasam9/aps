#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{
	/* code */
	int n = 1000000;
	int* primes = (int*) calloc(2*n+1, sizeof(int));
	long long int current_prime = 2;
	long long int i;
	int prime_count;
	int prime_found;

	// 0 means it is a prime other wise not
	primes[0] = 1;
	primes[1] = 1;

	int prime_list[78498];
	prime_list[0] = 2;
	prime_count = 1;


	while(current_prime <= n){

		// Marking all non primes
		for (i = current_prime*current_prime; i <= n; i+=current_prime){
			primes[i] = 1;
		}

		
		// Finding next prime
		prime_found = 0;
		for (i = current_prime+1; i <= n && !prime_found; ++i)
		{

			if(primes[i] == 0){
				current_prime = i;
				prime_list[prime_count] = current_prime;
				prime_count++;
				//printf("%d\n", current_prime);
				prime_found = 1;
			}
		}

		if(!prime_found)
			break;
		
	}


	int tc;
	scanf("%d",&tc);
	int num;

	int start,end,mid;

	while(tc > 0){
		scanf("%d",&num);
		int temp_num = num/2;

		// search num/2 in prime list
		start = 0;
		end = prime_count-1;
		while(end-start > 1){
			mid = (start+end)/2;	
			if(temp_num <= prime_list[mid]){
				end = mid;
			}else{
				start = mid+1;
			}
		}


		for (i = 0; i <= end ; ++i)
		{
			if(primes[(num- prime_list[i])] == 0 ){
				printf("%d %d\n", prime_list[i],num-prime_list[i]);
				break;
			}
		}

		tc--;
	}

	//printf("%d\n", prime_count);

	
	



	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);

	char input[200000];
	int head = -1;
	char* stack;

	while(tc > 0){
		scanf("%s",input);
		int length = strlen(input);

		// Initializing stack.
		stack = (char*) calloc(length, sizeof(char));
		head = -1;

		int i;
		char input_top,stack_top;

		for (i = 0; i < length; ++i)
		{	
			if(head >= 0){
				// If there are elements in stack
				input_top = input[i];
				stack_top = stack[head];

				if( (input_top == ')' && stack_top == '(') ||
					(input_top == ']' && stack_top == '[') ||
					(input_top == '}' && stack_top == '{')){
					head--;
				}else{
					head++;
					stack[head] = input_top;
				}
			}else{
				// Stack is empty
				head++;
				stack[head] = input[i];
			}
		}

		if(head != -1){
			printf("False\n");
		}else{
			printf("True\n");
		}
	tc--;
		
		

	}


	return 0;
}
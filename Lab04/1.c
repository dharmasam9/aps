#include "stdlib.h"
#include "stdio.h"

typedef struct link_node
{
	int value;
	struct link_node *next;
}node;


node* insert_node(node* head,int value){
	node* temp = (node *)calloc(1,sizeof(node));
	temp->value = value;

	if(head == NULL){
		head = temp;
	}else{
		node* tail = head;
		while(tail->next != NULL){
			tail = tail->next;
		}
		tail->next = temp;
	}
	return head;

}

void printLL(node* head){
	node* temp;
	temp = head;
	
	while(temp != NULL){
		printf("%4d", temp->value);
		temp = temp->next;
	}
	printf("\n");
}

node* deleteNode(node* head, int value){
	node* temp = head;
	node* temp_prev = head;

	while((temp != NULL) && (temp->value != value)){
		temp_prev = temp;
		temp = temp->next;
	}

	if(temp == NULL){
		// No element found
		return head;
	}else{
		// Element found
		if(temp_prev == temp){
			if(temp->next == NULL){
				free(temp);
				return NULL;
			}else{
				head = temp->next;
				temp->next = NULL;
				free(temp);
				return head;
			}
		}else{
			// Some where in between
			temp_prev->next = temp->next;
			temp->next = NULL;
			free(temp);
			return head;
		}
	}

}

node* insertAtPosition(node* head, int value, int loc){
	int index = 0;
	node* temp = head;
	node* temp_prev = head;

	while( (temp != NULL) && (index != loc)){
		temp_prev = temp;
		temp = temp->next;
		index++;
	}

	if(temp == NULL){
		printf("%d is greater than size of LL\n", loc);
	}else{
		node* new_node = (node*) calloc(1,sizeof(node));
		new_node->value = value;

		if(temp_prev == temp){
			head = new_node;
			head->next = temp;
			return head;
		}else{
			new_node->next = temp;
			temp_prev->next = new_node;

			return head;
		}

	}

}


node* reverseLL(node* head){
	if(head == NULL || head->next == NULL)
		return head;
	else{
		node* pivot1 = NULL;
		node* pivot2 = head;
		node* temp;

		while(pivot2 != NULL){
			temp = pivot2->next;
			pivot2->next = pivot1;
			pivot1 = pivot2;
			pivot2 = temp;
		}
		return pivot1;
	}
	
}


typedef struct dlink_node
{
	int value;
	struct dlink_node* next;
	struct dlink_node* prev;
}dnode;


dnode* insert_In_dlist(dnode* head,int elem){

	if(head == NULL){
		dnode* fresh = (dnode*)calloc(1,sizeof(dnode));
		fresh->value = elem;
		return fresh;
	}else{

		dnode* temp = head;
		dnode* temp_prev = head;
		while(temp != NULL && temp->value < elem){
			temp_prev = temp;
			temp = temp->next;
		}

		dnode* fresh = (dnode*)calloc(1,sizeof(dnode));
		fresh->value = elem;

		if(temp == head){
			fresh->next = temp;
			temp->prev = fresh;
			return fresh;
		}else if(temp == NULL){
			temp_prev->next = fresh;
			fresh->prev = temp_prev;
			fresh->next = NULL;
			return head;
		}else{
			// All nodes before temp has values <= elem
			fresh->next = temp;
			fresh->prev = temp->prev;
			fresh->prev->next = fresh;
			fresh->next->prev = fresh;
			return head;
		}

		return head;

	}

	

}

int main(int argc, char const *argv[])
{	
	/*
	node* head;
	head = insert_node(head,1);
	head = insert_node(head,3);
	head = insert_node(head,4);

	head = insertAtPosition(head,2,2);
	printLL(head);

	head = reverseLL(head);

	printLL(head);
	return 0;

	head = deleteNode(head,2);
	printLL(head);
	head = deleteNode(head,1);
	printLL(head);
	head = deleteNode(head,3);
	printLL(head);
	*/

	//int values[] = {-10,0,2,7,­15,16,32,50,64,101,128,399,400,1000};
	int values[14] = {-10,0,2,7,15,16,32,50,64,101,128,399,400,1000};

	dnode* d_head = (dnode*)calloc(1,sizeof(dnode));
	dnode* d_tail = d_head;

	d_head->value = values[0];
	int i;
	for (i = 1; i < 14; ++i)
	{
		dnode* temp = (dnode*)calloc(1,sizeof(dnode));
		temp->value = values[i];
		temp->prev = d_tail;
		d_tail->next = temp;

		d_tail = temp;
	}

	
	d_head = insert_In_dlist(d_head,-15);

	// Printing from front
	dnode* dummy = d_head;
	while(dummy != NULL){
		printf("%d ", dummy->value);
		dummy = dummy->next;
	}
	printf("\n");

	// Printing from back
	dummy = d_tail;
	while(dummy != NULL){
		printf("%d ", dummy->value);
		dummy = dummy->prev;
	}
	printf("\n");


	return 0;
}

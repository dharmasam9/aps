#include "stdlib.h"
#include "stdio.h"

typedef struct link_node
{
	int value;
	struct link_node *next;
}node;


node* insert_node(node* head,int value){
	node* temp = (node *)calloc(1,sizeof(node));
	temp->value = value;

	if(head == NULL){
		head = temp;
	}else{
		node* tail = head;
		while(tail->next != NULL){
			tail = tail->next;
		}
		tail->next = temp;
	}
	return head;

}

void printLL(node* head){
	node* temp;
	temp = head;
	
	while(temp != NULL){
		printf("%4d", temp->value);
		temp = temp->next;
	}
	printf("\n");
}



int main(int argc, char const *argv[])
{
	int m,n;
	node* list1 = NULL;
	node* list2 = NULL;

	scanf("%d",&m);

	int i,temp;
	for (i = 0; i < m; ++i)
	{
		scanf("%d",&temp);
		list1 = insert_node(list1, temp);
	}

	scanf("%d",&n);
	for (i = 0; i < n; ++i)
	{
		scanf("%d",&temp);
		list2 = insert_node(list2, temp);
	}

	//printLL(list1);
	//printLL(list2);

	//return 0;

	// Merge two lists

	node* merge_list_head = NULL;
	node* merge_list_tail = NULL;

	while(list1 != NULL && list2 != NULL){
		if(list1->value < list2->value){
			if(merge_list_head == NULL && merge_list_tail == NULL){
				merge_list_head = list1;
				merge_list_tail = list1;
				list1 = list1->next;
			}else{
				merge_list_tail->next = list1;
				merge_list_tail = list1;
				list1 = list1->next;
			}
		}else{
			if(merge_list_head == NULL && merge_list_tail == NULL){
				merge_list_head = list2;
				merge_list_tail = list2;
				list2 = list2->next;
			}else{
				merge_list_tail->next = list2;
				merge_list_tail = list2;
				list2 = list2->next;
			}
		}
	}

	if(list1 == NULL)
		merge_list_tail->next = list2;

	if(list2 ==  NULL)
		merge_list_tail->next = list1;

	printLL(merge_list_head);


	return 0;
}
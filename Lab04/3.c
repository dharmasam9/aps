#include "stdlib.h"
#include "stdio.h"


typedef struct link_node
{
	int value;
	struct link_node *next;
}node;


node* insert_node(node* head,int value){
	node* temp = (node *)calloc(1,sizeof(node));
	temp->value = value;

	if(head == NULL){
		head = temp;
	}else{
		node* tail = head;
		while(tail->next != NULL){
			tail = tail->next;
		}
		tail->next = temp;
	}
	return head;

}

void printLL(node* head){
	node* temp;
	temp = head;
	
	while(temp != NULL){
		printf("%4d", temp->value);
		temp = temp->next;
	}
	printf("\n");
}

node* reverseLL(node* head){
	if(head == NULL || head->next == NULL)
		return head;
	else{
		node* pivot1 = NULL;
		node* pivot2 = head;
		node* temp;

		while(pivot2 != NULL){
			temp = pivot2->next;
			pivot2->next = pivot1;
			pivot1 = pivot2;
			pivot2 = temp;
		}
		return pivot1;
	}
	
}


int main(int argc, char const *argv[])
{
	
	// Creating an input.

	node* head1 = NULL;
	node* head2 = NULL;
	node* common = NULL;

	int i;
	for (i = 0; i < 1; ++i)
		head1 = insert_node(head1,i);

	for (i = 0; i < 1; ++i)
		head2 = insert_node(head2,i+2);

	for (i = 0; i < 4; ++i)
		common = insert_node(common,i+7);

	node* temp;

	temp = head1;
	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next = common;


	temp = head2;
	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next = common;

	//printLL(head1);
	//printLL(head2);

	
	// Find the merge point;

	int l1_count = 0;
	temp = head1;
	while(temp != NULL){
		temp = temp->next;
		l1_count++;
	}

	int l2_count = 0;
	temp = head2;
	while(temp != NULL){
		temp = temp->next;
		l2_count++;
	}

	head1 = reverseLL(head1);

	int l1_and_l2_count = 0;
	temp = head2;
	while(temp != NULL){
		temp = temp->next;
		l1_and_l2_count++;
	}

	int merge_index = l2_count - (l1_count + l2_count - l1_and_l2_count+1)/2;

	//printf("%d %d %d %d\n",l1_count, l2_count, l1_and_l2_count-1,  merge_index);	
	temp = head2;
	while(merge_index != 0){
		temp = temp->next;
		merge_index--;
	}

	printf("%d\n",temp->value );


	return 0;
}
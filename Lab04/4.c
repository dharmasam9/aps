#include "stdlib.h"
#include "stdio.h"


typedef struct link_node
{
	int value;
	struct link_node *next;
}node;


node* insert_node(node* head,int value){
	node* temp = (node *)calloc(1,sizeof(node));
	temp->value = value;

	if(head == NULL){
		head = temp;
	}else{
		node* tail = head;
		while(tail->next != NULL){
			tail = tail->next;
		}
		tail->next = temp;
	}
	return head;

}


void printLL(node* head){
	node* temp;
	temp = head;
	
	while(temp != NULL){
		printf("%4d", temp->value);
		temp = temp->next;
	}
	printf("\n");
}


node* poly_LL(int* coef, int size){
	node* head = NULL;
	int i;
	for (i = 0; i < size; ++i)
		head = insert_node(head,coef[i]);

	return head;
}

double poly_compute(node* head,double x){
	double x_iter = 1.0;
	double answer = 0;

	node* temp = head;
	while(temp != NULL){
		answer += temp->value * x_iter;
		x_iter *= x;
		temp = temp->next;
	}
	return answer;
}

node* poly_Add(node* poly1, node* poly2){
	// Sizes will be same

	node* ret = NULL;
	node* temp1 = poly1;
	node* temp2 = poly2;

	while(temp1 != NULL){

		ret = insert_node(ret,temp1->value+temp2->value);

		temp1 = temp1->next;
		temp2 = temp2->next;
	}

	return ret;


}

int main(int argc, char const *argv[])
{
	int coef[5] = {1,2,3,4,5};

	node* head = poly_LL(coef, 5);
	node* head1 = poly_LL(coef, 5);

	node* head2 = poly_Add(head,head1);

	printLL(head2);
	printf("%lf",poly_compute(head,0.5));
	return 0;
}
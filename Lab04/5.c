#include "stdlib.h"
#include "stdio.h"


typedef struct link_node
{
	int value;
	struct link_node *next;
}node;


node* insert_node(node* head,int value){
	node* temp = (node *)calloc(1,sizeof(node));
	temp->value = value;

	if(head == NULL){
		head = temp;
	}else{
		node* tail = head;
		while(tail->next != NULL){
			tail = tail->next;
		}
		tail->next = temp;
	}
	return head;

}


void printLL(node* head){
	node* temp;
	temp = head;
	
	while(temp != NULL){
		printf("%4d", temp->value);
		temp = temp->next;
	}
	printf("\n");
}


node* poly_LL(int* coef, int size){
	node* head = NULL;
	int i;
	for (i = 0; i < size; ++i){
		if(coef[i] != 0){
			// Inserting index and value at that index only if coeff is not zero
			head  = insert_node(head,i);
			head = insert_node(head,coef[i]);
		}
	}

	return head;
}

double poly_compute(node* head,double x){
	double x_iter = 1.0;
	double answer = 0;
	int prev_index = 0;

	node* temp = head;
	int i;
	while(temp != NULL){
		for (i = 0; i<(temp->value - prev_index); ++i)
			x_iter *= x;
		
		prev_index = temp->value;
		temp = temp->next;
		answer += temp->value * x_iter;
		temp = temp->next;
	}
	return answer;
}

node* poly_Add(node* poly1, node* poly2){
	// Sizes will be same
	node* ret = NULL;

	node* temp_poly1 = poly1;
	node* temp_poly2 = poly2;	

	while(temp_poly1 != NULL && temp_poly2 != NULL){
		if(temp_poly1->value == temp_poly2->value){
			ret = insert_node(ret, temp_poly1->value);
			temp_poly1 = temp_poly1->next;
			temp_poly2 = temp_poly2->next;
			ret = insert_node(ret, temp_poly1->value+temp_poly2->value);
			temp_poly1 = temp_poly1->next;
			temp_poly2 = temp_poly2->next;
		}else if(temp_poly1->value < temp_poly2->value){
			ret = insert_node(ret,temp_poly1->value);
			temp_poly1 = temp_poly1->next;
			ret = insert_node(ret,temp_poly1->value);
			temp_poly1 = temp_poly1->next;
		}else{
			ret = insert_node(ret,temp_poly2->value);
			temp_poly2 = temp_poly2->next;
			ret = insert_node(ret,temp_poly2->value);
			temp_poly2 = temp_poly2->next;
		}
	}

	printLL(ret);


	// Empty temp_poly2
	while(temp_poly2 != NULL){
		ret = insert_node(ret, temp_poly2->value);
		temp_poly2 = temp_poly2->next;
	}
	

	// Empty temp_poly1
	while(temp_poly1 != NULL){
		ret = insert_node(ret, temp_poly1->value);
		temp_poly1 = temp_poly1->next;
	}
	

	return ret;

	


}

int main(int argc, char const *argv[])
{
	int coef[5] = {1,2,0,0,5};
	int coef1[5] = {0,0,3,4,0};

	node* head = poly_LL(coef, 5);
	node* head1 = poly_LL(coef1, 5);

	printLL(head);
	printLL(head1);

	node* head3 = poly_Add(head,head1);

	printLL(head3);

	double sum = poly_compute(head,0.5);
	printf("%lf\n", sum);
	
	
	return 0;
}

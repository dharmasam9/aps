#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void quick_sort(int* a,int start, int end){
	if(end-start <= 1){
		if(start < end){
			// Array of size 2
			int temp;
			if(a[start] > a[end]){
				// Size 2 not sorted
				temp = a[start];
				a[start] = a[end];
				a[end] = temp;
			}
		}
	}else{
		int pivot = start;
		int running_index = pivot;
		int last_index = pivot;
		int pivot_value = a[pivot];
		int temp;

		while(running_index <= end){
			if(a[running_index] < pivot_value){
				last_index++;
				temp = a[running_index];
				a[running_index] = a[last_index];
				a[last_index] = temp;
			}
			running_index++;
		}

		a[start] = a[last_index];
		a[last_index] = pivot_value;

		quick_sort(a, start, last_index-1);
		quick_sort(a, last_index+1, end);

	}
}

void print_array(int* a,int len){
	int i;
	for (i = 0; i < len; ++i)
	{
		printf("%3d", a[i]);
	}
	printf("\n");
}

int main(int argc, char const *argv[])
{


	int tc;
	scanf("%d",&tc);

	while(tc > 0){
		int m,n;
		scanf("%d",&m);
		scanf("%d",&n);

		int yis[m-1];
		int xis[n-1];
		int i;

		for (i = 0; i < m-1; ++i)
			scanf("%d",&yis[i]);

		for (i = 0; i < n-1; ++i)
			scanf("%d",&xis[i]);

		// Sort xis
		quick_sort(yis,0,m-2);
		quick_sort(xis,0,n-2);

		//print_array(yis,m-1);
		//print_array(xis,n-1);

		int mark_y,mark_x;
		int pieces_y,pieces_x;
		
		mark_y = m-2;
		mark_x = n-2;

		pieces_y = 1;
		pieces_x = 1;
		long long int ans = 0;

		while(mark_y >= 0 && mark_x >= 0){
			if(yis[mark_y] > xis[mark_x]){
				ans += (pieces_x*yis[mark_y]);
				ans = ans%1000000007;
				pieces_y++;
				mark_y--;
			}else if(yis[mark_y] < xis[mark_x]){
				ans += (pieces_y*xis[mark_x]);
				ans = ans%1000000007;
				pieces_x++;
				mark_x--;
			}else{
				long long int value = yis[mark_y];
				int count_y = 1;
				int count_x = 1;
				int done;

				done = 0;
				while(mark_x > 0 && !done){
					if(xis[mark_x] == xis[mark_x-1]){
						count_x++;
						mark_x--;
					}else{
						done = 1;
					}
				}
				mark_x--;

				done = 0;
				while(mark_y > 0 && !done){
					if(yis[mark_y] == yis[mark_y-1]){
						count_y++;
						mark_y--;
					}else{
						done = 1;
					}
				}
				mark_y--;


				long long int value1 = count_x*pieces_y + count_y*(pieces_x+count_x);
				long long int value2 = count_y*pieces_x + count_x*(pieces_y+count_y);

				if(value1 > value2){
					ans += value*value2;
				}else{
					ans += value*value1;
				}
				ans = ans%1000000007;

				pieces_x += count_x;
				pieces_y += count_y;
			}
		}

		while(mark_y >= 0){
			ans += pieces_x*yis[mark_y];
			ans = ans%1000000007;
			pieces_y++;
			mark_y--;
		}

		while(mark_x >= 0){
			ans += pieces_y*xis[mark_x];
			ans = ans%1000000007;
			pieces_x++;
			mark_x--;
		}

		printf("%lld\n", ans);

		// Sort yis
		tc--;

	}
	return 0;
	
}

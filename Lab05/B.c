#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{

	int tc;
	scanf("%d",&tc);

	char s[10001];

	while(tc > 0){
		scanf("%s",s);
		int size = strlen(s);
		int index = 0;
		int height = 0;
		int running_height = 0;


		while(index < size){
			if(s[index] == 'n'){
				running_height++;
				if(running_height > height){
					height = running_height;
				}
			}else{
				if(s[index-1] == 'l'){
					running_height--;
				}
			}
			
			index++;
		}

		printf("%d\n", height);

		tc--;

	}
	return 0;
}
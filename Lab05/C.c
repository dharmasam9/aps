#include <stdlib.h>
#include <stdio.h>

typedef struct node_s{
	int value;
	struct node_s* left;
	struct node_s* right;
}node;

node* insert_to_tree(node* root,int value){
	if(root == NULL){
		node* new_root = (node*) calloc(1,sizeof(node));
		new_root->value = value;
		return new_root;		
	}else{
		if(value < root->value){
			root->left = insert_to_tree(root->left,value);
		}else{
			root->right = insert_to_tree(root->right,value);
		}
		return root;
	}
}

void print_inorder(node* root){
	if(root == NULL){
		return;
	}else{
		print_inorder(root->left);
		printf("%d ", root->value);
		print_inorder(root->right);
	}
}

int find_lca(node* root,int p,int q){
	// If both are equal then root is the lca

	node* temp = root;
	int found = 0;

	while(!found){

		if(p < temp->value && q < temp->value){
			temp = temp->left;
		}else if(p > temp->value && q > temp->value){
			temp = temp->right;
		}else{
			found = 1;
		}

	}
	return temp->value;

	
}

int main(int argc, char const *argv[])
{
	node* root = NULL;
	int tc;

	scanf("%d",&tc);

	while(tc > 0){
		root = NULL;
		
		int m,k,p,q;
		int temp;

		scanf("%d",&m);
		scanf("%d",&k);

		while(m > 0){
			scanf("%d",&temp);
			root = insert_to_tree(root,temp);
			m--;
		}

		//print_inorder(root);
		//printf("\n");

		while(k > 0){
			scanf("%d",&p);
			scanf("%d",&q);

			temp = find_lca(root,p,q);
			printf("%d\n", temp);
			k--;
		}

		

		tc--;
	}


	return 0;
}

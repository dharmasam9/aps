#include <iostream>
#include <stdlib.h>

using namespace std;
 
typedef struct node
{
    int value;
    struct node *left;
    struct node *right;
    int height;
}node;

int getHeight(node* root, int value){
    if(root == NULL){
        return -1;
    }else if(root->value == value){
        return 0;
    }else if(value > root->value){
        int downHeight = getHeight(root->right, value);
        if(downHeight != -1)
            return 1+downHeight;
        else
            return downHeight;
    }else{
        int downHeight = getHeight(root->left, value);
        if(downHeight != -1)
            return 1+downHeight;
        else
            return downHeight;
    }
}
 

int get_node_height(node *root)
{
    if (root == NULL)
        return 0;
    else
        return root->height;
}

node *left_rotate(node *root)
{
    node *y = root->right;
    node *T2 = y->left;
 
    y->left = root;
    root->right = T2;
 
    root->height = max(get_node_height(root->left), get_node_height(root->right))+1;
    y->height = max(get_node_height(y->left), get_node_height(y->right))+1;
    return y;
}

node *right_rotate(node *root)
{
    node *y = root->left;
    node *T2 = y->right;
 
    y->right = root;
    root->left = T2;

    root->height = max(get_node_height(root->left), get_node_height(root->right))+1;
    y->height = max(get_node_height(y->left), get_node_height(y->right))+1;
    return y;
}
  
int get_height_diff(node *N)
{
    if (N == NULL)
        return 0;
    return get_node_height(N->left) - get_node_height(N->right);
}
 
node* insert_node(node* root, int value)
{
    if (root == NULL){
        node* temp = (node*)calloc(1, sizeof(node));
        temp->value = value;
        temp->height = 1;  
        return temp;
    }
 
    if (value < root->value){
        root->left  = insert_node(root->left, value);
    }else{
        root->right = insert_node(root->right, value);
    }
 
    root->height = max(get_node_height(root->left), get_node_height(root->right)) + 1;
 
    int difference = get_height_diff(root);
 
    if (difference > 1 && value < root->left->value){
        return right_rotate(root);
    }
 
    if (difference < -1 && value > root->right->value){
        return left_rotate(root);
    }
 
    if (difference > 1 && value > root->left->value)
    {
        root->left =  left_rotate(root->left);
        return right_rotate(root);
    }
 
    if (difference < -1 && value < root->right->value)
    {
        root->right = right_rotate(root->right);
        return left_rotate(root);
    }

    return root;
}
  
node* delete_node(node* root, int value, int &present)
{ 
    if (root == NULL)
        return root;
 
    if ( value < root->value ){
        root->left = delete_node(root->left, value, present);
    }else if( value > root->value ){
        root->right = delete_node(root->right, value, present);
    }else
    {
        if((root->left == NULL) || (root->right == NULL))
        {
            node *temp = root->left;
            if(temp == NULL){
                temp = root->right;
            }
            if(temp == NULL)
            {
                temp = root;
                root = NULL;
            }else {
                *root = *temp; 
            }
 
            free(temp);
            present = 1;
        }
        else
        {
            node* temp = root->right;
            while(temp->left != NULL){
                temp = temp->left;
            }
            root->value = temp->value;
            root->right = delete_node(root->right, temp->value, present);
        }
    }
 

    if (root == NULL)
       return root;
 
    root->height = max(get_node_height(root->left), get_node_height(root->right)) + 1;
 
    int difference = get_height_diff(root);
 
    if (difference > 1 && get_height_diff(root->left) >= 0){
        return right_rotate(root);
    }

    if (difference < -1 && get_height_diff(root->right) <= 0){
        return left_rotate(root);
    }
 
    if (difference > 1 && get_height_diff(root->left) < 0)
    {
        root->left =  left_rotate(root->left);
        return right_rotate(root);
    }
  
    if (difference < -1 && get_height_diff(root->right) > 0)
    {
        root->right = right_rotate(root->right);
        return left_rotate(root);
    }
 
    return root;
}
 
void print_pre_order(node *root)
{
    if(root != NULL)
    {
        cout << root->value << " ";
        print_pre_order(root->left);
        print_pre_order(root->right);
    }
}

int main()
{
  node *root = NULL;
  int present;
 

    int tc;
    cin >> tc;

    while(tc > 0){
        char c;
        int val;
        cin >> c >> val;

        switch(c){
            case 'I':
                root = insert_node(root, val);
                cout << "true" << endl;
                break;
            case 'D':
                present = 0;
                root = delete_node(root, val, present);
                if(present) cout << "true" << endl;
                else cout << "false" << endl;
                break;
            case 'H':
                cout << getHeight(root,val) << endl;
                break;
        }

        tc--;
    }

//  print_pre_order(root);
 
    return 0;
}
#include <iostream>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

typedef pair<int,int> node;


int main(int argc, char const *argv[])
{
	// Testing ming heap

	int tc;
	cin >> tc;

	int v,e;
	while(tc > 0){
		cin >> v >> e;
		vector<vector<int> > graph(v);
		priority_queue< node, vector<node >, greater<node > > my_heap;

		for (int i = 0; i < v; ++i)
			graph[i].resize(v,-1);

		int n1,n2,edgeLength, currentLength;
		for (int i = 0; i < e; ++i)
		{
			cin >> n1 >> n2 >> edgeLength;
			n1--;
			n2--;

			currentLength = graph[n1][n2];

			if(currentLength != -1){
				if(edgeLength < currentLength){
					graph[n1][n2] = edgeLength;
					graph[n2][n1] = edgeLength;
				}
			}else{
				graph[n1][n2] = edgeLength;
				graph[n2][n1] = edgeLength;				
			}
		}

		

		
		int start_node;
		cin >> start_node;
		start_node--;

		vector<int> distances(v, INT_MAX);
		vector<int> visited(v, 0);

		distances[start_node] = 0;
		my_heap.push(make_pair(distances[start_node], start_node));

		while(!my_heap.empty()){
			int dist = my_heap.top().first;
			int node = my_heap.top().second;
			my_heap.pop();

			visited[node] = 1;
			
			if(distances[node] < dist) continue;
			
			int edge_length;

			for (int i = 0; i < v; ++i)
			{
				edge_length = graph[node][i];
				if(edge_length != -1){
					
					if(distances[node] + edge_length < distances[i]){
						
						distances[i] = distances[node] + edge_length;
						
						my_heap.push(make_pair(distances[i], i));
						
					}
				
				}
			}
		}

		for (int i = 0; i < v; ++i){
			if(i != start_node){
				if(distances[i] == INT_MAX)
					cout << "-1 ";
				else
					cout << distances[i] << " ";

			}
				
		}
		cout << endl;
		
		tc--;
		
	}
	
	

	return 0;
}
#include <string> 
#include <sstream> 
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std; 

void min_cash_algorithm(vector<long long int> &net_amounts, int v, vector<pair<long long int,int> > &final_txns)
{

	int min_index = 0;
	int max_index = 0;

	for (int i = 1; i < v; ++i)
	{
		if(net_amounts[i] < net_amounts[min_index])
			min_index = i;

		if(net_amounts[i] > net_amounts[max_index])
			max_index = i;
	}
 
    if (net_amounts[max_index] == 0 && net_amounts[min_index] == 0)
        return;
 
    // Find the minimum of two net_amountss
    long long int min_value;
    if((-1*net_amounts[min_index]) < net_amounts[max_index]){
    	min_value = -1*net_amounts[min_index];
    }else{
    	min_value = net_amounts[max_index];
    }

    net_amounts[max_index] -= min_value;
    net_amounts[min_index] += min_value;

    final_txns.push_back(make_pair(min_value, (min_index*v)+max_index));

    min_cash_algorithm(net_amounts, v, final_txns);
}
 


int main() { 
	int tc;
	cin >> tc;

	int v;
	int n1,n2,debt;


	for (int k = 0; k < tc; ++k)
	{
		cin >> v;

		vector<vector<int> > graph(v);

		for (int i = 0; i < v; ++i)
		{
			graph[i].resize(v,0);
		}


		vector<long long int> net_amounts(v, 0);

		stringstream ss;
		string line;
		while(true){
			
			getline(cin, line);

			if(line[0] == '0' && line.size() == 1)
				break;

			if(line.size() > 0){
				ss<<line; 
				ss>> n1 >> n2 >> debt; 

				ss.str("");
				ss.clear();

				graph[n1][n2] = debt;

				net_amounts[n1] -= debt;
				net_amounts[n2] += debt;
			}
		}	

		// Intelligence goes here
		vector<pair<long long int,int> > final_txns;
		min_cash_algorithm(net_amounts, v, final_txns);

		sort(final_txns.begin(), final_txns.end());

		cout << "Case " << k << ":" << endl;
		int i = final_txns.size()-1;
		while(i >= 0)
		{
			// If amounts are equal sort first values
			long long int temp_amount = final_txns[i].first;
			vector<pair<int, long long int> > group;
			group.push_back(make_pair(final_txns[i].second, temp_amount));

			bool done = false;
			int j = i-1;
			while(j >=0 && !done){
				if(final_txns[j].first == temp_amount){
					group.push_back(make_pair(final_txns[j].second, temp_amount));
					j--;
				}else{
					done = true;
				}
			}

			sort(group.begin(), group.end());

			for (unsigned int iter = 0; iter < group.size(); ++iter)
			{
				cout << (group[iter].first)/v << " " << (group[iter].first)%v << " " << group[iter].second << endl;		
			}

			i = j;
			
		}

		if(final_txns.size() == 0)
			cout << "-1" << endl;
		
		
	}

	return 0; 
}
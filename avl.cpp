#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct node{
	int value;
	int height;
	struct node* left;
	struct node* right;
}node;

int getHeight(node* root, int value){
	if(root == NULL){
		return -1;
	}else if(root->value == value){
		return 0;
	}else if(value > root->value){
		int downHeight = getHeight(root->right, value);
		if(downHeight != -1)
			return 1+downHeight;
		else
			return downHeight;
	}else{
		int downHeight = getHeight(root->left, value);
		if(downHeight != -1)
			return 1+downHeight;
		else
			return downHeight;
	}
}

int getNodeHeight(node* root){
	if(root == NULL)
		return 0;
	else
		return root->height;
}



node* right_rotate(node* root){
	node* y = root->left;
	node* T3 = y->right;

	y->right = root;
	root->left = T3;

	root->height = max(getNodeHeight(root->left), getNodeHeight(root->right))+1;
	y->height = max(getNodeHeight(y->left), getNodeHeight(y->right));
	
	return y;

}

node* left_rotate(node* root){
	node* y = root->right;
	node* T2 = y->left;

	y->left = root;
	root->right = T2;

	root->height = max(getNodeHeight(root->left), getNodeHeight(root->right))+1;
	y->height = max(getNodeHeight(y->left), getNodeHeight(y->right));

	return y;
}


node* insert_node(node* root, int value){
	if(root == NULL){
		node* temp = new node;
		temp->value = value;
		temp->height = 1;
		return temp;
	}else{
		if(value > root->value){
			root->right = insert_node(root->right, value);
		}else{
			root->left = insert_node(root->left, value);
		}


		root->height = max(getNodeHeight(root->left),getNodeHeight(root->right))+1;

		int leftTreeHeight = 0, rightTreeHeight = 0;

		if(root->left != NULL) leftTreeHeight = root->left->height;
		if(root->right != NULL) rightTreeHeight = root->right->height;

		int diffHeight = leftTreeHeight - rightTreeHeight;

		

		if(diffHeight > 1 && value < root->left->value){
			//Left Left case
			return right_rotate(root);
		}

		if(diffHeight > 1 && value > root->left->value){
			// Left Right case			
			root->left = left_rotate(root->left);
			return right_rotate(root);
		}


		if(diffHeight < -1 && value > root->right->value){
			// Right Right case
			return left_rotate(root);
		}

		
		if(diffHeight < -1 && value < root->right->value){
			// Right Left case
			root->right = right_rotate(root->right);
			return left_rotate(root);
		}

		return root;

	}
}

// Get Balance factor of node N
int getBalance(node *N)
{
    if (N == NULL)
        return 0;
    return getNodeHeight(N->left) - getNodeHeight(N->right);
}

node* deleteNode(node* root, int value)
{ 
    if(root == NULL)
        return root;

    if(value < root->value){
        root->left = deleteNode(root->left, value);
    }else if(value > root->value){
        root->right = deleteNode(root->right, value);
    }else{
        if((root->left == NULL) || (root->right == NULL) )
        {
            node *temp = root->left ? root->left : root->right;
 
            // No child case
            if(temp == NULL)
            {
                temp = root;
                root = NULL;
            }
            else // One child case
             *root = *temp; // Copy the contents of the non-empty child
 
            free(temp);
        }
        else
        {
            // node with two children: Get the inorder successor (smallest
            // in the right subtree)
        	node* temp = root->right;

        	while(temp->left != NULL){
        		temp = temp->left;
        	}
 
            // Copy the inorder successor's data to this node
            root->value = temp->value;
 
            // Delete the inorder successor
            root->right = deleteNode(root->right, temp->value);
        }
    }
 
    // If the tree had only one node then return
    if (root == NULL)
      return root;
 
    // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
    root->height = max(getNodeHeight(root->left), getNodeHeight(root->right)) + 1;
 
    // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
    //  this node became unbalanced)
    int balance = getBalance(root);
 
    // If this node becomes unbalanced, then there are 4 cases
 
    // Left Left Case
    if (balance > 1 && getBalance(root->left) >= 0)
        return right_rotate(root);
 
    // Left Right Case
    if (balance > 1 && getBalance(root->left) < 0)
    {
        root->left =  left_rotate(root->left);
        return right_rotate(root);
    }
 
    // Right Right Case
    if (balance < -1 && getBalance(root->right) <= 0)
        return left_rotate(root);
 
    // Right Left Case
    if (balance < -1 && getBalance(root->right) > 0)
    {
        root->right = right_rotate(root->right);
        return left_rotate(root);
    }
 
    return root;
}

/*
node* delete_node(node* root, int value){
	if(root == NULL){
		return NULL;
	}else if(value > root->value){
		root->right = delete_node(root->right, value);
	}else if(value < root->value){
		root->left = delete_node(root->right, value);
	}else{
		// marker node equals to value.
        if( (root->left == NULL) || (root->right == NULL) )
        {
            struct node *temp = root->left ? root->left : root->right;
 
            // No child case
            if(temp == NULL)
            {
                temp = root;
                root = NULL;
            }
            else // One child case
             *root = *temp; // Copy the contents of the non-empty child
 
            free(temp);
        }else{
			// Two childs are present
			node* start = root->right;

			while(start->left != NULL){
				start = start->left;
			}

			root->value = start->value;

			root->right = delete_node(root->right, start->value);

		}

	}

	if(root == NULL)
		return NULL;

	root->height = max(getNodeHeight(root->left), getNodeHeight(root->right)) + 1;

	int leftTreeHeight = 0; 
	int rightTreeHeight = 0;


	if(root->left != NULL) leftTreeHeight = root->left->height;
	if(root->right != NULL) rightTreeHeight = root->right->height;

	int diffHeight = leftTreeHeight - rightTreeHeight;

	if(diffHeight > 1){
		node* temp = root->left;
		int diff_left_node = getNodeHeight(temp->left) - getNodeHeight(temp->right);

		if(diff_left_node >= 0)
			return right_rotate(root);
		else{
			root->left = left_rotate(root->left);
			return right_rotate(root);
		}
	}

	if(diffHeight < -1){
		node* temp = root->right;
		int diff_right_node = getNodeHeight(temp->left) - getNodeHeight(temp->right);

		if(diff_right_node <= 0)
			return left_rotate(root);
		else{
			root->right = right_rotate(root->right);
			return left_rotate(root);
		}
	}

	return root;

}
*/


void print_pre_order(node* root){
	if(root != NULL){
		cout << root->value << endl;
		print_pre_order(root->left);
		print_pre_order(root->right);
	}
}

int main(int argc, char const *argv[])
{

	node* root = NULL;
	root = insert_node(root, 9);
    root = insert_node(root, 5);
    root = insert_node(root, 10);
    root = insert_node(root, 0);
    root = insert_node(root, 6);
    root = insert_node(root, 11);
    root = insert_node(root, -1);
    root = insert_node(root, 1);
    root = insert_node(root, 2);

	print_pre_order(root);

	root = deleteNode(root, 10);

	cout << endl;

	print_pre_order(root);

	return 0;

	int tc;
	cin >> tc;

	

	while(tc > 0){
		char c;
		int val;
		cin >> c >> val;

		switch(c){
			case 'I':
				root = insert_node(root, val);
				break;
			case 'D':
				root = deleteNode(root, val);
				break;
			case 'H':
				break;
		}

		tc--;
	}

//	print_pre_order(root);
	return 0;
}
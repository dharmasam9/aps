/*
Author : Dharma teja
Program : Testing whether a number is perfect number or not

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a non-zero positive integer. Output 
YES if the number is perfect, otherwise output NO

Input Format : 
3
5
6
28

Output Format :
NO
YES
YES

Implementation Details :
Maination a varibale which stores the sum of all proper divisors.
As 1 is factor to any number, initialize it to 1. Loop from 2 to sqrt(n).
If a number i divides n then add i and n/i to the sum variable. Exit the 
loop after reaching sqrt(n) or sum variable is greater than n.
*/

#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[])
{

	int tc;
	cin >> tc;

	int n;
	while(tc > 0){
		tc--;

		cin >> n;

		int sum_of_factors = 1;

		for (int i = 2; i < sqrt(n) && sum_of_factors <= n; ++i)
		{
			if(n%i == 0){
				sum_of_factors += i;
				sum_of_factors += n/i;
			}
		}

		// Checking for sqrt(n)
		if(sqrt(n)*sqrt(n) == n)
			sum_of_factors += sqrt(n);

		if(sum_of_factors == n){
			cout << "YES" << endl;
		}else{
			cout << "NO" << endl;
		}
	}

	return 0;
}
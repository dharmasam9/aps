/*
Author : Dharma teja
Program : Implemetation of finding modulo of two numbers with out
using inbuild % operator.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains two integers n1,n2. Output the value of
n1%n2 for each input in a separate line

Input Format : 
4
0 1
2 4
4 3
4 2

Output Format :
0
2
1
0

Implementation Details :
If n1 is less than n2 return n1. Otherwise keep on removing n2 units
from n1 until n1 is less than n2. Once that happens return n1. In case
of negative numbers assign the sign of n1 to the answer.
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int recursive_mod(int n1,int n2){
	if(n1 < n2)
		return n1;
	else
		return recursive_mod(n1-n2, n2);
}

int iterative_mod(int n1,int n2){
	while(n1 >= n2){
		n1 -= n2;
	}

	return n1;
}

int main(int argc, char const *argv[])
{

	int tc;
	cin >> tc;

	int n1,n2;
	while(tc > 0){
		cin >> n1 >> n2;
		int answer = iterative_mod(abs(n1), abs(n2));

		if(n1 < 0)
			answer *= -1;

		//cout << answer << " " << n1%n2 << endl;
		cout << answer << endl;

		tc--;
	}

	return 0;
}
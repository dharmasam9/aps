/*
Author : Dharma teja
Program : Multiplication of two numbers using shift operators
shift operators

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains two integrs n1 and n2. Output an integer
for each test case.

Input Format : 
3
5 3
6 5
19 12

Output Format :
15
30
228

Implementation Details :
Decompose second number in to bits bn:b0. If bi is 1 then add a number
obtained by shift n1, i times left to the answer.
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int multiply(int n1, int n2){
	int answer = 0;
	int shift_index = 0;
	int shift_value = n1;

	while(n2 != 0){
		if(n2%2 == 1){
			answer += shift_value;
		}

		n2 = n2>>1;
		shift_value = shift_value<<1;
	}

	return answer;
}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int n1,n2;
	while(tc > 0){
		cin >> n1 >> n2;

		int answer = multiply(abs(n1), abs(n2));

		if(n1*n2 < 0)
			answer *= -1;

		cout << answer << endl;
		tc--;
	}
	return 0;
}

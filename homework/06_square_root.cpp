/*
Author : Dharma teja
Program : Finding square root of a number.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a float n. Output one float value
for each test case.

Input Format : 
2
3
4

Output Format :
2
1.732
2.000

Implementation Details :
The idea is that square of a number n should lie between (0,n).
So we start with the middle number and test whether square of that
number is greater or lesser than n. If it is greater thatn n, find
mid value of first half other wise find mid value of second half.
Repeat it until you reach an acceptable error.

*/

#include <iostream>
#include <cmath>

using namespace std;

float square_root(float n, float error_tolerance){
	float start_value = 0;
	float end_value = n;
	float mid_value;
	float calculated_n;

	do{
		mid_value = (start_value+end_value)/2;
		calculated_n = mid_value*mid_value;

		if(calculated_n < n){
			start_value = mid_value;
		}else{
			end_value = mid_value;
		}

	}while(fabs(calculated_n-n) > error_tolerance);

	return mid_value;
}

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	float error_tolerance = 1e-6; // answer has 1e-3 error

	float n;
	while(tc > 0){
		cin >> n;

		float answer = square_root(n, error_tolerance);
		cout << answer << endl;

		tc--;
	}


	return 0;
}
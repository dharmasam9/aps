/*
Author : Dharma tja
Program : Counting number of lines and characters in a file.

Explanation of input and output format:
Input will be a file path and is given as an argument to the 
executable. First line in the output should contain number of
lines in the file and second line in the ouput should  contain
number of characters in the line excluding space,tab,newline chars.

Input: Filepath as argument to executable.

Output Format :
5
15


Implementation Details :
Use the file reading funcions provided by c++. Check for ascii values
of space,tab and newline to exclude them from character count.
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
	ifstream input_file(argv[1]);
	string line_in_file;

	int line_count = 0;
	int char_count = 0;

	while(getline(input_file, line_in_file)){
		line_count++;

		for (int i = 0; i < line_in_file.size(); ++i)
		{
			if(!(line_in_file[i] == ' ' || line_in_file[i] == '\t')){
				char_count++;
			}
		}
	}

	input_file.close();

	cout << line_count << endl;
	cout << char_count << endl;

	return 0;
}
/*
Author : Dharma tja
Program : Sort a given set of integers

Explanation of input and output format:
First line in the input contains number of test cases.
Each test case has two lines. First line is the size of
the array, second line has n integers separated by space.
For each test case output the larget, smallest, average
values of array.

Input Format : 
2
9
8 7 9 2 1 3 6 4 11
5
2 1 5 8 4

Output Format :
11 1 5.66
8 1 4.0

Implementation Details :
Traverse the array and update minimum if encountered a
value less than current value. Follow similar approach for
maximum. Maintain a variable sum and add array element values
as you traverse. Finally divide it by size to get average.
*/

#include <iostream>
#include <climits>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;
		int values[n];

		for (int i = 0; i < n; ++i)
		{
			cin >> values[i];
		}

		int min_value = INT_MAX;
		int max_value = INT_MIN;
		float sum = 0;

		for (int i = 0; i < n; ++i)
		{
			if(values[i] < min_value)
				min_value  = values[i];

			if(values[i] > max_value)
				max_value = values[i];

			sum += values[i];
		}

		cout << max_value << " " << min_value << " " << sum/n << endl;


		tc--;
	}

	return 0;
}
/*
Author : Dharma tja
Program : Finding median of an array

Explanation of input and output format:
First line in the input contains number of test cases.
Each test case has two lines. First line is the size of
the array, second line has n integers separated by space.
For each test case output the median of the array.

Input Format : 
2
9
9 8 7 1 2 3 6 4 5
5
5 1 2 4 3

Output Format :
5
3

Implementation Details :
Pick a number at random for array and partition array in to
two groups GL and GH respectively. GL has elements lesser 
than chosen random element and G2 has elements greater than 
the chosen random element. If length of GL is greater than
n/2 find n/2 the element in GL recursively, other wise find
n/2 - len(GL) th element in GH recursivery.
*/

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int find_median(int n, int values[]){
	int median_index = n/2;
	int start_index = 0;
	int end_index = n-1;
	int random_index;

	bool median_found = false;

	while(!median_found){
		int temp;

		random_index = rand()%(end_index-start_index+1) + start_index;

		//Swap with the start_index;
		temp = values[random_index];
		values[random_index] = values[start_index];
		values[start_index] = temp;

		int less_index = start_index;

		for (int i = start_index+1; i <= end_index; ++i)
		{
			if(values[i] < values[start_index]){
				less_index++;

				// Movind less values to front
				temp = values[i];
				values[i] = values[less_index];
				values[less_index] = temp;
			}
		}


		//Swapping first with last less than
		temp = values[less_index];
		values[less_index] = values[start_index];
		values[start_index] = temp;

		if(median_index == less_index)
			median_found = true;
		else{
			if(median_index > less_index){
				start_index = less_index+1;
			}else{
				end_index = less_index-1;
			}
		}

	}

	return values[median_index];

}

int main(int argc, char const *argv[])
{
	srand(time(NULL));

	int tc;
	cin >> tc;

	int n;
	while(tc > 0){
		cin >> n;

		int values[n];

		for (int i = 0; i < n; ++i)
			cin >> values[i];

		int median  = find_median(n, values);
		cout << median << endl;
		tc--;
	}



	return 0;
}
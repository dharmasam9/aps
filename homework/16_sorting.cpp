/*
Author : Dharma tja
Program : Sort a given set of integers

Explanation of input and output format:
First line in the input contains number of test cases.
Each test case has two lines. First line is the size of
the array, second line has n integers separated by space.
For each test case output the sorted array separated by space.

Input Format : 
2
9
9 8 7 1 2 3 6 4 5
5
5 1 2 4 3

Output Format :
1 2 3 4 5 6 7 8 9
1 2 3 4 5

Implementation Details :
Using Quick sort. Details can be found at wikipedia!
*/

#include <iostream>
#include <stdlib.h>

using namespace std;


void swap(int values[],int index1, int index2){
	int temp = values[index1];
	values[index1] = values[index2];
	values[index2] = temp;
}

void quick_sort(int values[], int start, int end){
	if((end-start) < 1){
		return;
	}else{
		int random_index = rand()%(end-start+1) + start;

		swap(values, start, random_index);
		int less_index = start;
		int pivot = values[start];

		for (int i = start+1; i <= end; ++i)
		{
			if(values[i] < pivot){
				less_index++;
				swap(values, i, less_index);
			}
		}

		swap(values, start, less_index);

		quick_sort(values, start, less_index-1);
		quick_sort(values, less_index+1, end);

		return;
	}
}

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;

		int values[n];
		for (int i = 0; i < n; ++i)
			cin >> values[i];

		quick_sort(values, 0, n-1);

		for (int i = 0; i < n; ++i)
			cout << values[i] << " ";
			cout << endl;
		
		tc--;
	}
	return 0;
}
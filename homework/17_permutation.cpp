/*
Author : Dharma teja
Program : Display all permutations of a string.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a string. Output all permutations in each line.

Input Format : 
2
012
te

Output Format :
012
012
021
102
120
210
201

te
et

Implementation Details :
permutations(c1c2c3) = c1perutations(c2c3) U c2perutations(c1c3) U c3perutations(c12)
Uses recursion to print permutations
*/

#include <iostream>

using namespace std;

void print_permutations(string s1, string s2, int &count){

	if(s2.size() == 1){
		count++;
		//cout << count << " " << s1+s2 << endl;
		cout << s1+s2 << endl;
	}else{
		char temp;
		for (int i = 0; i < s2.size(); ++i)
		{
			// Swap i with 0.
			temp  = s2[i];
			s2[i] = s2[0];
			s2[0] = temp;

			print_permutations(s1+s2[0] ,s2.substr(1,s2.size()-1), count);

			// Swapping it back.
			temp  = s2[i];
			s2[i] = s2[0];
			s2[0] = temp;
			
		}
	}

	
}

int main(int argc, char const *argv[])
{
	int count;
	int tc;
	cin >> tc;
	while(tc > 0){
		string s;
		cin >> s;

		count = 0;

		print_permutations("", s, count);
		cout << endl;
		tc--;
	}
	return 0;
	
}
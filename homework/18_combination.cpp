/*
Author : Dharma teja
Program : Display all k combinations of a string.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a string and number k separated by space.

Input Format : 
2
0123 2
dharm 3

Output Format :
01
02
03
12
13
23

dha
dhr
dhm
har
ham
arm

Implementation Details :

*/
#include <iostream>
#include <string>
using namespace std;

void print_combinations(int level, int position, int k, string s, string &current){

	if(level == k){
		cout << current << endl;
	}else{
		for (int i = position; i < s.size(); ++i)
		{
			current[level] = s[i];
			print_combinations(level+1, i+1, k, s, current);
		}	
	}

}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;
	int k, level;

	while(tc > 0){
		string s;
		
		cin >> s >> k;
		string current(k,'0');
		print_combinations(0, 0, k, s, current);

		cout << endl;

		tc--;
	}
	return 0;
}

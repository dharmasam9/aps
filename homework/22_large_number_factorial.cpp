/*
Author : Dharma teja
Program : Factorial of large numbers

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a non-zero positive integer. Output 
the factorial value for each test case.

Input Format : 
3
20
50


Output Format :
2432902008176640000
30414093201713378043612608166064768844377641568960512000000000000

Implementation Details :
In order to implement these I have written two modules. First is 
to add two strings. Second is to multiply a string with digit.
Second module is used to calculate value at each classical level
of multiplication. First is to accumulate values at indivudual
levels.
*/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

string add_strings(string n1, string n2){
	int min_size;
	string first_part;

	if(n1.size() < n2.size()){
		min_size = n1.size();
		first_part = n2.substr(0, n2.size()-min_size);
	}else{
		min_size = n2.size();
		first_part = n1.substr(0, n1.size()-min_size);
	}
	

	string result(min_size,'0');
	int carry = 0;

	int dec_n1, dec_n2, local_add;
	for (int i = 0; i < min_size; ++i)
	{
		dec_n1 = n1[n1.size()-1-i] - '0';
		dec_n2 = n2[n2.size()-1-i] - '0';

		local_add = dec_n1 + dec_n2 + carry;

		result[min_size-1-i] = (char)(local_add%10 + 48);
		carry = local_add/10;
	}



	if(carry == 0){
		return first_part+result;
	}else{
		//add carry to first part and then concatenate
		for (int i = first_part.size()-1; i >= 0; i--)
		{
			dec_n1 = first_part[i] - '0';
			local_add = dec_n1+carry;

			first_part[i] = (char)(local_add%10 + 48);
			carry = local_add/10;
		}

		if(carry != 0){
			string temp(1,(char)(carry + 48));
			first_part = temp + first_part;
		}

		return first_part +result;
	}
}

string multiply_string_with_digit(string n1, int scale){
	int carry = 0;
	int dec_n1, local_mul;
	for (int i = n1.size()-1; i >=0; i--)
	{
		dec_n1 = n1[i]-'0';
		local_mul = dec_n1*scale + carry;

		n1[i] = (char)(local_mul%10 + 48);
		carry = local_mul/10;
	}

	if(carry == 0)
		return n1;
	else
		return (char)(carry+48) + n1;
}

string multiply_two_strings(string n1, string n2){
	string result(1,'0');

	for (int i = n2.size()-1; i >=0; i--)
	{
		string leading_zero_string(n2.size()-1-i,'0');
		string level_result = 
		multiply_string_with_digit(n1, (int)(n2[i]-'0')) + leading_zero_string;
	
		result = add_strings(result, level_result);
	}

	return result;
}




int main(int argc, char const *argv[])
{
	int factorial = atoi(argv[1]);

	string answer = "1";
	string multiplier = "1";

	for (int i = 1; i <= factorial; ++i)
	{
		answer = multiply_two_strings(answer, multiplier);
		multiplier = add_strings(multiplier,"1");
	}

	cout << answer << endl;

	return 0;
}

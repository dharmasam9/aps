/*
Author : Dharma teja
Program : Take a bit segment from number2 and replace it in number1 at that position.

Explanation of input and output format:
First line contatins number of test cases. Following that each
line has four numbers (x,p,n,y). 
Output the modified value.

Input Format : 
1
7 0 2 4

Output Format :
5

Implementation Details :
Iteratively traverse both segments. At each step there can be four cases
(0,0), (1,1), (0,1), (1,0). Handle last two cases, by adding or subtrsacting
number at the current location.
*/

#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	// Checking shift ops
	int tc;
	cin >> tc;

	int x,p,n,y;
	while(tc>0){
		cin >> x >> p >> n >> y;
		int new_x = x;
		int current_value = 1<<p;;

		x = x>>p;
		y = y>>p;

		for (int i = 0; i < n; ++i)
		{
			if(x%2 == 0){
				if(y%2 != 0){
					new_x += current_value;
				}
			}else{
				if(y%2 == 0){
					new_x -= current_value;
				}
			}

			x = x>>1;
			y = y>>1;
			current_value = current_value<<1;
		}

		

		cout << new_x << endl;

		tc--;

	}

	
	return 0;
}
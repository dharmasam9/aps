/*
Author : Dharma teja
Program : Invert bits of a segment in the number.

Explanation of input and output format:
First line contatins the input number. Second line contains
number of test cases. Each following line has two numbers, first
is the index from left from where inversion should take place. Second
is the length of the segment to left.
Output the modified number in each line


Input Format : 
7
2
3 3
1 1

Output Format :
63
5

Implementation Details :
As many test cases are there on one input, it is efficient to calculate
the bit notation of number initially. Once that is found for each testcase,
edit the copy of the input number according and output.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char const *argv[])
{
	// Checking shift ops
	int n, tc;
	cin >> n >> tc;

	int* bits = new int[32];
	int copy_n = n;
	int running_index = 31;

	while(copy_n != 0){
		if(copy_n%2 == 0){
			bits[running_index] = 0;
		}else{
			bits[running_index] = 1;
		}

		copy_n = copy_n>>1;
		running_index--;
	}

	/*
	for (int i = 0; i < 32; ++i)
	{
		cout << bits[i];
	}
	cout << endl;
	*/

	int bit_base, bit_offset;
	int new_n;
	while(tc > 0){

		new_n = n;
		cin >> bit_base >> bit_offset;

		int cur_value = 1<<bit_base;

		for (int i = bit_base; i < bit_base+bit_offset; ++i)
		{
			if(bits[31-i]){
				// Bit is set
				new_n -= cur_value;
			}else{
				// Bit is not set
				new_n += cur_value;
			}			
			cur_value = cur_value<<1;
		}

		cout << new_n << endl;
		
		tc--;
	}
	

	return 0;
}
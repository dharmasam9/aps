/*
Author : Dharma teja
Program : Building a pascal triangle of height k

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a non-zero positive integer. Output 
the pascal triangle.

Input Format : 
2
2
4

Output Format :
 1
1 1

   1
  1 1
 1 2 1
1 3 3 1
Implementation Details :
Initialize a k length array. Set the value of first element to 1.
At each level L, add adjacencet values and update the array.
*/
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int L;
	while(tc > 0){
		cin >> L;

		int* values = new int[L];
		int empty_length = L-1;

		// Initalizing first value to 1 and printing
		values[0] = 1;
		for (int i = 0; i < empty_length; ++i)
			cout << " ";
			cout << "1" << endl;

		for (int i = 1; i < L; ++i)
		{	
			int temp_prev = values[0];
			int temp_cur;
			for (int j = 1; j < L; ++j)
			{
				temp_cur = values[j];
				values[j] += temp_prev;
				temp_prev = temp_cur;

			}
			values[L] = 1;

			empty_length--;

			for (int k = 0; k < empty_length; ++k)
				cout << " ";

			for (int k = 0; k <=i; ++k)
				cout << values[k] << " ";
				cout << endl;
		}

		tc--;
	}
	return 0;
}
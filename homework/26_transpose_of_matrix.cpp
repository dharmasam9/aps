/*
Author : Dharma teja
Program : Inversion of a matrix

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case has two parts. First line is an integer n ,the size 
of the matrix. Following n lines containg n integers forming
the input matrix. Output the transpose of the matrix

Input Format : 
2
2
1 2
3 4
3
1 0 2
0 2 3
1 0 5

Output Format :
1 3
2 4

1 0 1
0 2 0
2 3 5
Implementation Details :
Traverse the upper triangle of the matrix and transpose them 
one by one.
*/
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		
		int n;
		cin >> n;

		int values[n][n];

		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
					cin >> values[i][j];
			}
		}

		// Transpose
		int temp;
		for (int i = 0; i < n; ++i)
		{
			for (int j = i+1; j < n; ++j)
			{
				temp = values[i][j];
				values[i][j] = values[j][i];
				values[j][i] = temp;
			}
		}

		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				cout << values[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;


		tc--;
	}
	return 0;
}
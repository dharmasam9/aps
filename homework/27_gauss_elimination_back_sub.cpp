/*
Author : Dharma teja
Program : Findind x in Ax=b using gauss elimination and back 
substitution.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case has two parts. First line is an integer n ,the size 
of the matrix. Following n lines containg (n+1) integers forming
the input matrix A and b vector.Output the solution of Ax = b

Input Format : 
2
2
1 2
3 4
3
1 0 2
0 2 3
1 0 5

Output Format :
-1
1.5

5.66667
3
0.666667

Implementation Details :
Standard gauss elmination algorithm. Refer to wikipedia for more.
*/
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		
		int n;
		cin >> n;

		float values[n][n+1];

		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j <= n; ++j)
			{
					cin >> values[i][j];
			}
		}


		for (int i = 0; i < n; ++i)
		{	
			// Making values[i][i] 1
			float divisor = values[i][i];
			for (int j = i; j < n+1; ++j)
				values[i][j] /= divisor;

			
			for (int j = i+1; j < n; ++j)
			{	
				if(values[j][i] != 0){
					float multiplier = values[j][i];
					for (int k = i; k < n+1; ++k)
					{
						values[j][k] -= (multiplier * values[i][k]);
					}	
				}
			}
			

		}

		// Finding solution by back substitution.
		float* solution = new float[n];

		for (int i = n-1; i >=0; i--)
		{
			float accum = 0; 
			for (int j = i+1; j < n; ++j)
			{
				accum += values[i][j]*solution[j];
			}

			solution[i] = (values[i][n] - accum)/values[i][i];
		}

		for (int i = 0; i < n; ++i)
		{
			cout << solution[i] << endl;
		}
		cout << endl;

		tc--;
	}
	return 0;
}
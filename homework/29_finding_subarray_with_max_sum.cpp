/*
Author : Dharma tja
Program : Find the subarray which has the maximum sum.

Explanation of input and output format:
First line in the input contains number of test cases.
Each test case has two lines. First line is the size of
the array, second line has n integers separated by space.
For each test case output the sum of the subarray.

Input Format : 
1
5
-1 -2 3 -1 4

Output Format :
6

Implementation Details :
Observation 1: Solution array will not end or start with a negative number.
Consider two subarrays with all positve numbers.Let us say these two subarrays
are on either side of a subarray with all negative numbers. Now by including
negative subarray if we can get maximum do it, other wise do not. Now find the 
first positive number from the start and keep on adding consecutive number until 
the cumulative sum is less than 0. At each step update maximum value. If cumulative 
sum is negative, find the next positive number and continue the process until you 
reach the end.
*/

#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;
		vector<int> values;

		int temp;
		for (int i = 0; i < n; ++i)
		{
			cin >>temp;
			values.push_back(temp);
		}

		// maximum subarray intelligence.
		int max_value = 0;
		int current_sum = 0;
		int i = 0;

		bool found = false;
		while(i<n){
			// Finding first positive number
			while(!found && i<n){
				if(values[i] > 0)
					found = true;
				else
					i++;
			}

			// If it cannot find a positive number
			if(i >= n)
				break;

			current_sum = values[i];
			if(current_sum > max_value) max_value = current_sum;
			i++;
			
			while(current_sum >= 0 && i<n){
				current_sum += values[i];
				i++;

				if(current_sum > max_value) max_value = current_sum;
			}
		}

		cout << max_value << endl;



		tc--;
	}
	return 0;
}
/*
Author : Dharma teja
Program : Reversng a linked list.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case contains two parts. First part has one line with
n , where n denotes number of elements . Second part has 1 line with
n elements separated by space. Output the reverse of the numbers.

Constraints:
Use linked lists and not array

Input Format : 
1
3
2 3 4

Output Format :
4 3 2

Implementation Details :

*/

#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct node{
	int value;
	struct node* next;
}node;

node* reverse_list(node* root){
	if(root->next == NULL){
		return root;
	}else if(root->next->next == NULL){
		root->next->next = root;
		node* temp = root->next;
		root->next = NULL;
		return temp;
	}else{
		node* temp = reverse_list(root->next);
		root->next->next = root;
		root->next = NULL;
		return temp;
	}
}

node* insert_node(node* root, int value){
	node* temp = (node*)calloc(1,sizeof(node));
	temp->value = value;

	if(root == NULL){	
		return temp;
	}else{
		node* runner_node = root;
		while(runner_node->next != NULL){
			runner_node = runner_node->next;
		}
		runner_node->next = temp;
		return root;
	}
}


void print_list(node* root){
	while(root != NULL){
		cout << root->value << " ";	
		root = root->next;
	}
	cout << endl;
}


int main(int argc, char const *argv[])
{

	int tc;
	cin >> tc;

	while(tc > 0){
		node* root = NULL;

		int n;
		cin >> n;
		int temp;

		for (int i = 0; i < n; ++i)
		{
			cin >> temp;
			root = insert_node(root,temp);
		}

		root = reverse_list(root);
		print_list(root);

		tc--;
	}

	
	return 0;
}


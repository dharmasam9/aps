/*
Author : Dharma teja
Program : Concatenating two linked lists

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case contains three parts. First part has one line with
n1 and n2 , where n1 denotes number of elements in array1 and n2
denots number of elements in array2. Second part has 1 line with
n1 elements separated by space. Third part has 1 line with
n2 elements separated by space.

Output the concatenated list.

Constraints:
Use linked lists and not array

Input Format : 
1
3 4
2 3 4
4 3 2

Output Format :
2 3 4 4 3 2

Implementation Details :

*/


#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct node{
	int value;
	struct node* next;
}node;

node* insert_node(node* root, int value){
	node* temp = (node*)calloc(1,sizeof(node));
	temp->value = value;

	if(root == NULL){	
		return temp;
	}else{
		node* runner_node = root;
		while(runner_node->next != NULL){
			runner_node = runner_node->next;
		}
		runner_node->next = temp;
		return root;
	}
}


void print_list(node* root){
	while(root != NULL){
		cout << root->value << " ";	
		root = root->next;
	}
	cout << endl;
}


int main(int argc, char const *argv[])
{

	int tc;
	cin >> tc;

	while(tc > 0){
		node* root1 = NULL;
		node* root2 = NULL;

		int n1,n2;
		cin >> n1 >> n2;
		int temp;

		for (int i = 0; i < n1; ++i)
		{
			cin >> temp;
			root1 = insert_node(root1,temp);
		}

		for (int i = 0; i < n2; ++i)
		{
			cin >> temp;
			root2 = insert_node(root2,temp);
		}

		node* end_of_list1 = root1;

		while(end_of_list1->next != NULL){
			end_of_list1 = end_of_list1->next;
		}

		end_of_list1->next = root2;

		print_list(root1);


		tc--;
	}

	
	return 0;
}


/*
Author : Dharma teja
Program : Checking wellformed paranthesis using stack

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a string which belongs to language {(,)}*
output YES if given string is wellformed.

Input Format : 
2
(())()
))(())((

Output Format :
YES
NO

Implementation Details :
Traverse through the string. If you encounter '(' or'{' or '[' add it to stack.
If you encounter any of the closing brace, check whether top element of stack
is a match or not. If not, the string is not well formed. Even if there are
no elements in the stack, the string is not well formed.
*/
#include <iostream>
#include <stack>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	stack<char> paranth_stack;

	while(tc > 0){
		string s;
		cin >> s;

		bool well_formed = true;

		for (int i = 0; i < s.size() && well_formed; ++i)
		{
			if(s[i] == '(' || s[i] == '{' || s[i] == '['){
				paranth_stack.push(s[i]);
			}else{
				if(paranth_stack.empty()){
					well_formed = false;
				}else{
					char top_elem = paranth_stack.top();
					paranth_stack.pop();

					if(!((top_elem == '(' && s[i] == ')') || (top_elem == '{' && s[i] == '}')
						|| (top_elem == '[' && s[i] == ']')))
					{
						well_formed = false;
					}
				}
			}
		}

		string answer = well_formed==true?"YES":"NO";
		
		cout << answer << endl;

		tc--;
	}
	return 0;
}
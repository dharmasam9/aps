/*
Author : Dharma teja
Program : Converting infix notation to postfix notation.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a expression in infix notation. Output 
the post-fix notation.

Input Format : 
2
((a*b)+c)
((a*b)/(c+d))

Output Format :
ab*c+
ab*cd+/

Implementation Details :
Use stack for non-operands. When you encounter a closing brace output
pop all elments from stack till first encounter of '('.
*/


#include <iostream>
#include <string>
#include <stack>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		string s;
		cin >> s;

		stack<char> non_operand_stack;
		bool done;

		for (int i = 0; i < s.size(); ++i)
		{
			switch(s[i]){
				case '(':
					non_operand_stack.push(s[i]);
					break;
				case ')':
					done = false;	
					while(!done){
						if(non_operand_stack.top() == '(')
							done = true;
						else
							cout << non_operand_stack.top();
						non_operand_stack.pop();
					}
					break;
				case '+':
				case '-':
				case '/':
				case '*':
					non_operand_stack.push(s[i]);
					break;
				default:
					cout << s[i];
			}
		}

		cout << endl;

		tc--;
	}
	return 0;
}
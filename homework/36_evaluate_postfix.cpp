/*
Author : Dharma teja
Program : Converting infix notation to postfix notation.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
following line contains a expression in postfix notation. Output 
the evaluated value of postfix notation.

Input Format : 
1
56*32+/

Output Format :
6

Implementation Details :
Use stack for operands. When you encounter an operator take top two
elements from stack, perform the operation and push the new value
on to the stack.
*/

#include <iostream>
#include <stack>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		string s;
		cin >> s;

		stack<float> operand_stack;

		for (int i = 0; i < s.size(); ++i)
		{

			if(s[i] >= '0' && s[i] <= '9'){
				operand_stack.push(s[i]-'0');
			}else{
				float top1 = operand_stack.top();
				operand_stack.pop();
				float top2 = operand_stack.top();
				operand_stack.pop();

				float new_value;
				switch(s[i]){
					case '+':
						new_value = top1+top2;
						break;
					case '*':
						new_value = top1*top2;
						break;
					case '-':
						new_value = top2-top1;
						break;
					case '/':
						new_value = top2/top1;
						break;
				}
				operand_stack.push(new_value);
			}
		}

		cout << operand_stack.top() << endl;

		tc--;
	}
	return 0;
}
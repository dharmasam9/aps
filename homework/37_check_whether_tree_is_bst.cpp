/*
Author : Dharma teja
Program : Checking whether given given binary tree is BST or not

Explanation of input and output format:
Randomly generates a binary tree

Output:
Outputs YES if the tree is binary, other wise output NO.


Implementation Details :
Test the root and then recursively test left and right tree.
If there is any discrepancy output NO.
*/

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

using namespace std;

typedef struct node{
	int value;
	struct node* left;
	struct node* right;
}node;

node* insert_it_to_tree(node* root, int input){
	if(root == NULL){
		node* temp = (node*)calloc(1,sizeof(node));
		temp->value = input;
		return temp;
	}else{
		if(input < root->value){
			root->left = insert_it_to_tree(root->left, input);
		}else{
			root->right = insert_it_to_tree(root->right, input);
		}
		return root;
	}
}

void generate_random_tree(node* root, int range){
	if(root != NULL){
		root->value = rand()%range;
		generate_random_tree(root->left, range);
		generate_random_tree(root->right, range);
	}
}

void check_tree_for_bst(node* root, vector<int> &values){
	if(root != NULL){
		check_tree_for_bst(root->left, values);
		values.push_back(root->value);
		check_tree_for_bst(root->right, values);
	}
}

// In-Order Traversal
void in_order_traversal(node* root){
	if(root != NULL){
		in_order_traversal(root->left);
		cout << root->value << " ";
		in_order_traversal(root->right);
	}
}

int main(int argc, char const *argv[])
{
	srand(time(NULL));
	
	int n = 5;
	int range = 100;

	node* root = NULL;

	for (int i = 0; i < n; ++i)
	{
		root = insert_it_to_tree(root,rand()%range);
	}

	generate_random_tree(root, range);

	vector<int> values;
	check_tree_for_bst(root, values);

	bool is_bst  = true;
	for (int i = 1; i < n && is_bst; ++i)
	{
		if(values[i] < values[i-1])
			is_bst = false;
	}

	if(is_bst)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;


	return 0;
}


/*
Author : Dharma teja
Program : Find height and diameter of a Binary tree

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case has two lines.First line contains n , number of 
elements in tree. Second line contains the elements used
to form a binary search tree. Output height and diameter for
each test case.

Input Format : 
2
15
13 7 2 1 6 4 3 5 8 10 9 11 12 14 15	
13
6 2 1 4 3 5 7 8 12 10 9 11 13

Output Format :
6 9
6 9

Implementation Details :
Idea is first to find the height of left sub tree and
right sub tree. Then max(l,r)+1 is the height of the root.
Start with the root and follow it recursively.

*/

#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct node{
	int value;
	struct node* left;
	struct node* right;
}node;

node* insert_it_to_tree(node* root, int input){
	if(root == NULL){
		node* temp = (node*)calloc(1,sizeof(node));
		temp->value = input;
		return temp;
	}else{
		if(input < root->value){
			root->left = insert_it_to_tree(root->left, input);
		}else{
			root->right = insert_it_to_tree(root->right, input);
		}
		return root;
	}
}

int find_height_and_diameter(node* root, int &diameter){
	if(root == NULL){
		return 0;
	}else{
		int left_value = find_height_and_diameter(root->left, diameter);
		int right_value = find_height_and_diameter(root->right, diameter);

		if(left_value+right_value+1 > diameter){
			diameter = left_value + right_value + 1;
		}

		int root_value = left_value > right_value?left_value+1:right_value+1;
		return root_value;
	}
}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;

		node* root = NULL;

		int input;
		for (int i = 0; i < n; ++i)
		{
			cin >> input;
			root = insert_it_to_tree(root,input);
		}


		int diameter = 0;
		int height = find_height_and_diameter(root, diameter);

		cout << height << " " << " " << diameter << endl;


		tc--;
	}
	return 0;
}

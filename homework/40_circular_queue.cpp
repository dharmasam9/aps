/*
Author : Dharma teja
Program : Circular queue implementation

Type of program : Skill demonstration

Implementation Details:
Use an array along with two indices. One index is the position at
which new element should be inserted. Another index is the position
from which element has to removed. If index overflows mod it with the
capacity of queue.
*/
#include <iostream>
using namespace std;

#define QUEUE_CAPACITY 10

typedef struct queue{
	int size;
	int enqueue_index;
	int dequeue_index;
	int* values;
}queue;

queue get_a_queue(){
	queue temp_queue;
	temp_queue.size = 0;
	temp_queue.enqueue_index = 0;
	temp_queue.dequeue_index = 0;
	temp_queue.values = new int[QUEUE_CAPACITY];

	return temp_queue;
}

void enqueue(queue &circ_queue, int value){
	if(circ_queue.size >= QUEUE_CAPACITY){
		cout << "QUEUE iS FULL" << endl;
	}else{
		circ_queue.values[circ_queue.enqueue_index]	= value;
		circ_queue.enqueue_index = (circ_queue.enqueue_index+1)%QUEUE_CAPACITY;	
		circ_queue.size++;
	}
}

int dequeue(queue &circ_queue){
	if(circ_queue.size == 0){
		cout << "NOTHING TO DEQUUE" << endl;
	}else{
		int value = circ_queue.values[circ_queue.dequeue_index];
		circ_queue.dequeue_index = (circ_queue.dequeue_index+1)%QUEUE_CAPACITY;
		circ_queue.size--;
		return value;
	}
}

int main(int argc, char const *argv[])
{	
	queue circ_queue = get_a_queue();
	for (int i = 0; i < 20; ++i)
	{
		enqueue(circ_queue,i);
		if(i%3 == 0)
			cout << dequeue(circ_queue) << endl;
	}	
	return 0;
}
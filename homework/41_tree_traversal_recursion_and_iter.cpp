/*
Author : Dharma teja
Program : Inorder/Preorder/Postorder Traversals using Recursion/Iterative techniques.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case has two lines.First line contains n , number of 
elements in tree. Second line contains the elements used
to form a binary search tree. Output all the traversals for each test case.

Input Format : 
2
15
13 7 2 1 6 4 3 5 8 10 9 11 12 14 15	
13
6 2 1 4 3 5 7 8 12 10 9 11 13

Output Format :
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
1 2 3 4 5 6 7 8 9 10 11 12 13

Implementation Details :
For defintions refer wikipedia. Traversal can be achieved using recursion.
In case of pre-order print the root first and call traversal function
on left, followed by right. Similary for other traversals.

*/


#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct node{
	int value;
	struct node* left;
	struct node* right;
}node;

node* insert_it_to_tree(node* root, int input){
	if(root == NULL){
		node* temp = (node*)calloc(1,sizeof(node));
		temp->value = input;
		return temp;
	}else{
		if(input < root->value){
			root->left = insert_it_to_tree(root->left, input);
		}else{
			root->right = insert_it_to_tree(root->right, input);
		}
		return root;
	}
}

// Pre-Order Traversal
void pre_order_traversal(node* root){
	if(root != NULL){
		cout << root->value << " ";
		pre_order_traversal(root->left);
		pre_order_traversal(root->right);
	}
}

// In-Order Traversal
void in_order_traversal(node* root){
	if(root != NULL){
		in_order_traversal(root->left);
		cout << root->value << " ";
		in_order_traversal(root->right);
	}
}

// Post-Order Traversal
void post_order_traversal(node* root){
	if(root != NULL){
	 post_order_traversal(root->left);
	 post_order_traversal(root->right);
	 cout << root->value << " ";
	}
}


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n;
		cin >> n;

		node* root = NULL;

		int input;
		for (int i = 0; i < n; ++i)
		{
			cin >> input;
			root = insert_it_to_tree(root,input);
		}

		// Infrastructure
		node** information_stack = (node**)calloc(n, sizeof(node*));
		int max_value;
		int stack_size;

		// Pre-order
		pre_order_traversal(root);
		cout << endl;


		// In order
		in_order_traversal(root);
		cout << endl;

		stack_size =0;
		information_stack[stack_size] = root;
		stack_size++;
		max_value = -1;

		while(stack_size != 0){
			node* cur = information_stack[stack_size-1];
			if(cur->left != NULL && cur->left->value > max_value){
				information_stack[stack_size] = cur->left;
				stack_size++;
			}else{
				cout << cur->value << " ";
				stack_size--;

				if(cur->value > max_value){
					max_value = cur->value;
				}

				if(cur->right != NULL){
					information_stack[stack_size] = cur->right;
					stack_size++;
				}
			}
		}

		cout << endl;

		
		//post_order_traversal(root);
		//cout << endl;
	

		tc--;
	}
	return 0;
}

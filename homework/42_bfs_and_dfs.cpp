/*
Author : Dharma teja
Program : BFS and DFS

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case contains two parts. First part has one line with
n and e, where n denotes number of vertices and e denotes
number of edges. Second part has e lines, each line has two
values n1,n2.

Input Format : 
1
5 6
0 1
0 2
1 3
2 4
3 4
1 4

< Skill Demonstration>

Implementation Details :
Breadth First Search, Depth First Search Algorithms.
*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

#define WHITE 0
#define GREY 1
#define BLACK 2

void dfs_visit(int start_vertex,
				vector<vector<int> > &graph,
				vector<int> &vertex_colors){

	// Making node visited
	vertex_colors[start_vertex] = GREY;

	for (unsigned int i = 0; i < graph[start_vertex].size(); ++i)
	{
		int neighbour = graph[start_vertex][i];
		if(vertex_colors[neighbour] == WHITE){
			vertex_colors[neighbour] = GREY;
			dfs_visit(neighbour, graph, vertex_colors);
		}
	}

	vertex_colors[start_vertex] = BLACK;
}

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n,e;
		cin >> n >> e;

		vector<vector<int> > graph(n);
		int n1,n2;
		for (int i = 0; i < e; ++i)
		{
			cin >> n1 >> n2;
			graph[n1].push_back(n2);
			graph[n2].push_back(n1);
		}

		// DFS
		vector<int> vertex_colors(n, WHITE);
		for (int i = 0; i < n; ++i)
		{
			if(vertex_colors[i] == WHITE){
				dfs_visit(i, graph, vertex_colors);
			}
		}

		// BFS
		queue<int> nodes_queue;
		vector<int> visited(n,WHITE);
		vector<int> distances(n,-1);

		for (int i = 0; i < n; ++i)
		{
			if(visited[i] == WHITE){
				nodes_queue.push(i);
				distances[i] = 0;

				while(!nodes_queue.empty()){
					int cur_node = nodes_queue.front();
					nodes_queue.pop();

					// Making it visited
					visited[cur_node] = GREY;

					for (int i = 0; i < graph[cur_node].size(); ++i)
					{
						int neighbour = graph[cur_node][i];
						if(visited[neighbour] == WHITE){
							nodes_queue.push(neighbour);
							visited[neighbour] = GREY;
							distances[neighbour] = distances[cur_node]+1;
						}
					}

					visited[cur_node] = BLACK;
				}
			}
		}

		for (int i = 0; i < n; ++i)
			cout << distances[i] << " ";
			cout << endl;

		tc--;
	}


	return 0;
}
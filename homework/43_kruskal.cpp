/*
Author : Dharma teja
Program : Finding Minimum Spanning Tree of an undirected graph.

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case contains two parts. First part has one line with
n and e, where n denotes number of vertices and e denotes
number of edges. Second part has e lines, each line has three
values n1,n2,c denoting an undirected edge between n1 to n2 with c cost.

Output the value of MST using prims and kruskal separated by space.

Input Format : 
1
3 3
0 1 1
0 2 4
1 2 2

Output Format :
3

Implementation Details :
I have used vector of sets to maintain and merge paths while inserting
new edge in to the graph.
*/
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

typedef pair<int,int> node;


int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	int n,e;
	while(tc > 0){
		cin >> n >> e;
		vector<pair<int,int> > edges;

		int n1,n2,edgeLength, currentLength;
		for (int i = 0; i < e; ++i)
		{
			cin >> n1 >> n2 >> edgeLength;
			edges.push_back(make_pair(edgeLength, n1*n+n2));
		}

		sort(edges.begin(), edges.end());

		vector<set<int> > paths;
		int mst_weight = 0;

		for (int i = 0; i < e; ++i)
		{
			n1 = edges[i].second/n;
			n2 = edges[i].second%n;

			int n1_path = -1;
			int n2_path = -1;

			for (int j = 0; j < paths.size(); ++j)
			{
				if(paths[j].find(n1) != paths[j].end()){
					n1_path = j;
				}

				if(paths[j].find(n2) != paths[j].end()){
					n2_path = j;
				}
			}

			bool is_cycle = false;
			if(n1_path == n2_path && n1_path != -1){
				is_cycle = true;
			}

			if(!is_cycle){

				mst_weight += edges[i].first;
				if(n1_path == -1 && n2_path == -1){
					set<int> temp;
					temp.insert(n1);
					temp.insert(n2);
					paths.push_back(temp);
				}else{
					if(n1_path != -1 && n2_path != -1){
						if(n1_path != n2_path){
							// Move set2 to set1 and delete set2
							for(set<int>::iterator it=paths[n2_path].begin(); it!=paths[n2_path].end(); it++)
								paths[n1_path].insert(*it);

							paths.erase(paths.begin()+n2_path);
						}
					}else{
						if(n1_path != -1)
							paths[n1_path].insert(n2);
						else
							paths[n2_path].insert(n1);
					}
				}

			}

		}

		cout << mst_weight << endl;

		// Perform prims


		//Perform kruskal
		
		tc--;
	}
}
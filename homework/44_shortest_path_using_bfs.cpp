/*
Author : Dharma teja
Program : Dijsktra Algorithm

Explanation of input and output format:
First line of the input contatins number of test cases. Each
test case contains three parts. First part has one line with
n and e, where n denotes number of vertices and e denotes
number of edges. Second part has e lines, each line has three
values n1,n2,c denoting an undirected edge from n1 to n2
Third part has only one line with source vertex as input.

Output the shortest path length to all the vertices from source.

Input Format : 
1
5 6
0 1
0 2
1 3
2 4
3 4
1 4
0

Output Format :
0 1 1 2 2

Implementation Details :
BFS algo from source vertex along with distance vector.
*/
#include <iostream>
#include <queue>

using namespace std;

int main(int argc, char const *argv[])
{
	int tc;
	cin >> tc;

	while(tc > 0){
		int n,e;
		cin >> n >> e;

		vector<vector<int> > adjacencyList(n);
		int n1,n2;
		for (int i = 0; i < e; ++i)
		{
			cin >> n1 >> n2;
			adjacencyList[n1].push_back(n2);
			adjacencyList[n2].push_back(n1);
		}

		int source;
		cin >> source;

		queue<int> nodes_queue;
		vector<int> visited(n,0);
		vector<int> distances(n,-1);

		// Enqueuing source;
		nodes_queue.push(source);
		distances[source] = 0;

		while(!nodes_queue.empty()){
			int cur_node = nodes_queue.front();
			nodes_queue.pop();

			// Making it visited
			visited[cur_node] = 1;

			for (int i = 0; i < adjacencyList[cur_node].size(); ++i)
			{
				int neighbour = adjacencyList[cur_node][i];
				if(visited[neighbour] == 0){
					nodes_queue.push(neighbour);
					visited[neighbour] = 1;
					distances[neighbour] = distances[cur_node]+1;
				}
			}

			visited[cur_node] = 2;
		}

		for (int i = 0; i < n; ++i)
			cout << distances[i] << " ";
			cout << endl;


		tc--;
	}
	return 0;
}
/*
Author: V.Dharma teja
Input:

<books>
	<book>
		<author>Siddharth</author>
		<title>Introduction to xpath</title>
	</book>
	<book lang="en">
		<author>James</author>
		<title>Introduction to XML</title>
	</book>
</books>
EOF
/books/book[1]/title
/books/book[1]/author

Output:

Introduction to XML
James

*/

#include <iostream>
#include <sstream>
#include <map>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

class XML{
public:
	string tag;
	string content;
	string childContent;
	vector<XML> children;

	XML();
	XML(string s);

	void setContent(string s);
	void populateTag();
	void populateChildContent();
	void populateChildren();
	static string findTag(string s);
	
};

string findTag(string s){
	int end = 0;

	while(s[end] != '>' && s[end] != ' ')
		end++;

	return s.substr(1, end-1);	
}

void XML::populateChildren(){
	// Recurse only if it is xml
	if(childContent[0] == '<'){
		// Find each child and recurese
		int start = 0;
		int end = 0;

		string temp = childContent;
		bool done = false;

		while(!done){
			int first_open = 0;
			int second_open = 0;
			int first_close = 0;
			int second_close = 0;

			first_open = 0;
			first_close = first_open;

			int space_ind = -1;
			while(temp[first_close] != '>'){
				first_close++;
				if(temp[first_close] == ' ')
					space_ind = first_close; 
			}

			string tag;
			if(space_ind != -1)
				tag = temp.substr(first_open+1, space_ind-1);
			else
				tag = temp.substr(first_open+1, first_close-first_open-1);

			string notag = temp.substr(first_close+1, temp.size()-first_close);

			size_t found = notag.find(tag);

			second_open =  (first_close-first_open+1) + (found -2);
			second_close = (first_close-first_open+1) + found+ tag.size();

			string childNode = temp.substr(0,second_close+1);

			/*
			cout << "***" << endl;
			cout <<  tag << endl;
			cout << childNode << endl;
			*/
			
			
			XML x;
			x.setContent(childNode);
			x.populateTag();
			x.populateChildContent();
			x.populateChildren();

			children.push_back(x);
			
			// Changing temp
			temp = temp.substr(childNode.size(), temp.size()-childNode.size());
			//cout << temp << endl;

			if(temp.size() == 0)
				done = true;

		}


	}

}

XML::XML(){

}

XML::XML(string s){
	this->tag = s;
}

void XML::setContent(string s){
	this->content = s;
}


void XML::populateTag(){
	int end = 0;

	while(content[end] != '>' && content[end] != ' ')
		end++;

	tag = content.substr(1, end-1);
}

void XML::populateChildContent(){
	int piv1 = 0;
	while(content[piv1] != '>')
		piv1++;
	piv1++;

	int piv2 = content.size()-1;
	while(content[piv2] != '<')
		piv2--;
	piv2--;

	childContent = content.substr(piv1, piv2-piv1+1);


}



vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

int main(int argc, char const *argv[])
{
	
	// Read content to a string and create class.
	string input = "";
	string temp;
	bool done = false;

	while(!done){
		getline(cin,temp);

		// Remove spaces from front and back.
		int start = 0;
		int end = temp.size()-1;

		while(temp[start] == '\t') start++;
		while(temp[end] == '\t') end--;

		temp = temp.substr(start, end-start+1);
		
		if(temp != "EOF")
			input += temp;
		else
			done = true;
	}

	//cout << input << endl;



	// Root xml
	XML root;
	root.setContent(input);
	root.populateTag();
	root.populateChildContent();

	//cout << root.content <<  endl;
	//cout << root.tag << endl;
	//cout << root.childContent << endl;
	root.populateChildren();
	// Find tag
	string query;

	while(getline(cin,query)){	
	
		//string query = "/books/book[123]/title";
		//string query = "/books/book[0]/title";

		vector<string> tokens = split(query,'/');

		vector<XML> nodes;
		nodes.push_back(root);
		XML answer = root;

		bool pos = true;

		for (int i = 1; i < tokens.size() && pos; ++i)
		{
			if(tokens[i][tokens[i].size()-1] != ']'){
				bool found = false;

				int j = 0;
				int count_ind = -1;
				for (int j = 0; j < nodes.size() && !found; ++j)
				{
					if(nodes[j].tag == tokens[i]){
						found = true;
						count_ind = j;
					}
				}

				if(!found)
					pos = false;
				else{
					answer = nodes[count_ind];
					nodes =  nodes[count_ind].children;
				}

			}else{
				string last = split(tokens[i],'[')[1];
				string intString = last.substr(0,last.size()-1);
				string tok = tokens[i].substr(0,tokens[i].size()-2-intString.size());


				int index = atoi(intString.c_str());
				

				int j = 0;
				int count = -1;
				int count_ind = -1;
				while(j < nodes.size() && count != index){
					//cout << nodes[j].tag << endl;
					if(nodes[j].tag == tok){
						count++;
						count_ind = j;
					}
					j++;
				}



				if(count != index)
					pos = false;
				else{
					answer = nodes[count_ind];
					nodes =  nodes[count_ind].children;
				}
			}

			
		}

		if(pos)
			cout << answer.childContent << endl;
		else
			cout << "NO" << endl;
	}


	return 0;
}
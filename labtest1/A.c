#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{

	int shift;
	char buf[1010];

	scanf("%d\n",&shift);
	while(fgets(buf, sizeof(buf), stdin) != NULL){
		int* counts = (int*) calloc(26,sizeof(int));
		int i;

		int size = strlen(buf);

		for (i = 0; i < size; ++i)
		{
			int value = (int) buf[i];
			int new_value;
			if(value >= 65 && value <= 90){
				counts[value-65]++;
				new_value = ((value-65)+shift)%26;
				buf[i] = (char) (new_value+65);
			}
				

			if(value >= 97 && value <= 122){
				counts[value-97]++;
				new_value = ((value-97)+shift)%26;
				buf[i] = (char) (new_value+97);
			}
		}


		int alpha_count = 0;

		for ( i = 0; i < 26; ++i)
		{
			if(counts[i] != 0)
				alpha_count++;
		}

		//printf("%d\n", alpha_count);

		if(alpha_count == 26)
			printf("%s", buf);
		else
			printf("-1\n");

	}




	return 0;
}
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[])
{
	int tc;
	scanf("%d",&tc);


	while(tc > 0){
		int n;
		scanf("%d",&n);

		int* values = (int*) calloc(n, sizeof(int));

		int i;
		for (i = 0; i < n; ++i)
			scanf("%d",&values[i]);

		long long int value = 1;

		int stack[n];
		int stack_size = 0;

		stack[0] = values[0];
		stack_size++;


		for (i = 1; i < n; ++i)
		{
			if(values[i] > stack[stack_size-1]){
				// Come down until stack is empty or stack[r] > cur
				int done =0;
				int run = stack_size-1;
				while(!done && stack_size > 0){
					if(stack[run] > values[i]){
						done = 1;
						stack[stack_size] = values[i];
						stack_size++;
					}else{
						value = value * values[i];
						value = value % 1000000007;
						stack_size--;

						// printf("%d %lld\n", i,value);

					}
					run--;
				}

				// All elemnets less than index i are less
				if(done == 0 || stack_size == 0){
					stack[stack_size] = values[i];
					stack_size++;
				}
			
				
			}else{
				stack[stack_size] = values[i];
				stack_size++;
			}
		}

		
		printf("%lld\n", value);

		
		

		tc--;
	}

	return 0;
}
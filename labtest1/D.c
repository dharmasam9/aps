#include <stdio.h>
#include <stdlib.h>

void quick_sort(int* a, int start, int end){
	if(end-start <= 1){
		if(start < end){
			// Array of size 2
			int temp;
			if(a[start] > a[end]){
				// Size 2 not sorted
				temp = a[start];
				a[start] = a[end];
				a[end] = temp;
			}
		}
	}else{
		int pivot = start + rand()%(end-start+1);
		int temp;

		// Exchange pivot and start;
		temp = a[pivot];
		a[pivot] = a[start];
		a[start] = temp;


		pivot = start;

		int running_index = pivot;
		int last_index = pivot;
		int pivot_value = a[pivot];		

		while(running_index <= end){
			if(a[running_index] < pivot_value){
				last_index++;

				temp = a[running_index];
				a[running_index] = a[last_index];
				a[last_index] = temp;

			}
			running_index++;
		}

		a[start] = a[last_index];
		a[last_index] = pivot_value;


		quick_sort(a, start, last_index-1);
		quick_sort(a, last_index+1, end);

	}
}

void print_array(int* values, int n){
	int i;
	for (i = 0; i < n; ++i)
	{
		printf("%d ",values[i]);
	}
	printf("\n");
}

int search(int* values,int start, int end, int elem){
	int mid = (start+end)/2;

	//printf("%d %d %d\n",start,mid,end );

	while(start != end){
		
		if(elem > values[mid]){
			start = mid+1;
		}else if(elem == values[mid]){
			return mid;
		}else{
			end = mid;
		}
		mid = (start+end)/2;
	}

	return start;


}

int main(int argc, char const *argv[])
{
	int n;
	int values[2000];

	while(1){
		scanf("%d",&n);

		if(n == 0){
			break;
		}

		int i,j;
		for (i = 0; i < n; ++i)
		{
			scanf("%d",&values[i]);
		}

		quick_sort(values, 0, n-1);
		// print_array(values,n);

		int sums[n*n];

		int num_pos = 0;

		for (i = 0; i < n-2; ++i)
		{	
			for (j = i+1; j < n-1; ++j)
			{
				// printf("%d %d\n", values[i], values[j] );
				
				int sum = values[i]+values[j];

				// First elem >= sum
				int index = search(values,j+1, n-1, sum);
				//printf("%d\n", index);

				if(values[index] == sum)
					index++;

				int greater = (n-1) - index +1;

				//printf("%d %d %d\n", total,greater,less);
				if(greater > 0)	num_pos += greater;

				if(index == n-1 && values[n-1] < sum) 
					num_pos--;

							
			}
		}

		printf("%d\n", num_pos);


	}

	


	return 0;
}
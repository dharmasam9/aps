#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

#include <cstdio>
#include <cstdlib>

using namespace std;

string print(string s){
	return s;
}

string toUpper(string s){
	for (int i = 0; i < s.length(); ++i)
	{
		s[i] = (char)toupper(s[i]);
	}
	return s;
}


string toLower(string s){
	for (int i = 0; i < s.size(); ++i)
	{
		s[i] = tolower(s[i]);
	}
	return s;
}

string append_a(string s){
	return s+"a";
}

string reverse(string s){
	char c;
	int i = 0;
	int j = s.size()-1;

	while(i < j){
		c = s[i];
		s[i] = s[j];
		s[j] = c;

		i++; j--;
	}
	return s;
}


void get_tokens_in_reverse(string s, vector<string> &tokens){

	// Handling empty lines
	if(s.compare("") == 0) return;

	int end = s.size()-1;
	for (int i = end; i >= 0; i--){
		if(s[i] == ' '){
			tokens.push_back(s.substr(i+1,end-i));
			end = i-1;
		}
	}

	tokens.push_back(s.substr(0,end+1));
}

string eval_func(string input, string op, map<string, vector<vector<string> > > &functions){
	if(op.compare("print") == 0){
		return print(input);
	}else if(op.compare("reverse") == 0){
		return reverse(input);
	}else if(op.compare("tolower") == 0){
		return toLower(input);
	}else if(op.compare("toupper") == 0){
		return toUpper(input);
	}else if(op.compare("tolower") == 0){
		return toLower(input);
	}else if(op.compare("append_a") == 0){
		return append_a(input);
	}else{
		// Evaluate function;
		vector<vector<string> > statements =  functions[op];
		string current = input;

		for (int i = 0; i < statements.size(); ++i)
		{
			if(statements[i][statements[i].size()-1].compare("print") != 0){

				if(statements[i][0].compare("str") == 0){
					for (int j = 1; j < statements[i].size(); ++j)
						current = eval_func(current, statements[i][j], functions);
				}else{
					// Double quoted string
					string temp = statements[i][0];
					for (int j = 1; j < statements[i].size(); ++j)
						temp = eval_func(temp, statements[i][j], functions);
				}

			}else{
				// Print statement
				string temp;
				if(statements[i][0].compare("str") == 0)
					temp = current;
				else
					temp = statements[i][0].substr(1,statements[i][0].size()-2);

				for (int j = 1; j < statements[i].size(); ++j)
					temp = eval_func(temp, statements[i][j], functions);
				cout << temp << endl;
			}
		}

		return current;		
	}

}

bool check_dblquote_string(string s){
	if(s[0] != '\"') return false;
	if(s[s.size()-1] != '\"') return false;

	/*
	for (int i = 1; i < s.size()-1; ++i)
	{
		if(!isalpha(s[i])) return false;
	}
	*/

	return true;
}

bool is_valid_main_statement(vector<string> &tokens, set<string> &func_tokens){

	// Ends with a quoted string and has token size > 1
	if(check_dblquote_string(tokens[0]) && tokens.size() > 1){
		// Add functions to set
		for(int i=1;i<tokens.size();i++)
			func_tokens.insert(tokens[i]);
		return true;
	}else{
		return false;
	}
}

bool is_valid_function_statement(vector<string> &tokens, set<string> &func_tokens){
	// Ends with str or a double quoted string and tokens are greater than size 1
	if((tokens[0].compare("str") == 0 || check_dblquote_string(tokens[0])) && tokens.size() > 1){
		// Add functions to set
		for(int i=1;i<tokens.size();i++)
			func_tokens.insert(tokens[i]);
		return true;
	}else{
		return false;
	}

}


int main(int argc, char const *argv[])
{

	string s;

	vector<vector<string> > statements;
	map<string,vector<vector<string> > > functions;
	set<string> func_tokens;

	func_tokens.insert("print"); func_tokens.insert("toupper"); func_tokens.insert("tolower");
	func_tokens.insert("append_a"); func_tokens.insert("reverse");

	// For syntax error
	bool is_error = false;

	while(getline(cin,s) && !is_error){

		vector<string> tokens;
		get_tokens_in_reverse(s,tokens);

		// Only non-empty lines and non-comments
		if(tokens.size() > 0 && tokens[tokens.size()-1][0] != '%'){
			if(tokens[tokens.size()-1].compare("define") != 0){
				if(is_valid_main_statement(tokens, func_tokens)){
					statements.push_back(tokens);
				}else{
					is_error = true; // Invalid main statement
				}
			}else{
				// Valid function first line
				if(tokens.size() == 2){
					string fname = tokens[0];
					vector<vector<string> > f_stats;

					bool done = false;
					vector<vector<string> > temp_key;
					while(!done && getline(cin,s)){
						vector<string> tokens2;
						get_tokens_in_reverse(s,tokens2);

						if(tokens2.size() > 0 && tokens2[tokens2.size()-1][0] != '%'){
							if(tokens2[tokens2.size()-1].compare("end") == 0){
								// If end found
								done = true;
								if(tokens2.size() > 1) is_error = true; // Line with end has >1 tokens
							}else{
								if(is_valid_function_statement(tokens2, func_tokens)){
									temp_key.push_back(tokens2);
								}else{
									is_error = true; // Invalid function statement
								}
							}	
						}
					}

					if(!done) is_error = true; // No "end" to function defintion

					functions[fname] = temp_key;	
				}else{
					is_error = true;
				}
			}
		}
	}

	for(set<string>::iterator it=func_tokens.begin(); it !=func_tokens.end() && !is_error; it++){
		// Checking whether function call in statement is valid or not.
		if(((*it).compare("print") != 0) && ((*it).compare("reverse") != 0) && ((*it).compare("append_a") != 0) 
			&& ((*it).compare("toupper") != 0) && ((*it).compare("tolower") != 0) && (functions.find(*it) == functions.end())){
			is_error = true;
		}
	}

	if(is_error){
		cout << "ERROR" << endl;
	}else{
		string current;
		for (int i = 0; i < statements.size(); ++i)
		{
			// Evaluate the function
			current = statements[i][0].substr(1,statements[i][0].size()-2);
			for (int j = 1; j < statements[i].size(); ++j)
			{
				current = eval_func(current, statements[i][j], functions);
			}

			// Print only if it is a print statement.
			if(statements[i][statements[i].size()-1].compare("print") == 0)
				cout << current << endl;
		}	
	}

	/*
	for (int i = 0; i < statements.size(); ++i)
	{
		for (int j = 0; j < statements[i].size(); ++j)
			cout << statements[i][j] << " ";
			cout << endl;	
	}

	for(map<string,vector<vector<string> > >::iterator it = functions.begin(); it != functions.end(); it++ ){
		cout << "func(" << it->first << ")-> " ;

		for (int i = 0; i < it->second.size(); ++i)
			for (int j = 0; j < it->second[i].size(); ++j)
				cout << it->second[i][j] << " ";
		
		cout << endl;
	}
	*/
	

	



	
	return 0;
}